

TEMPLATE	= subdirs
SUBDIRS  = main hstarter



### begin mac-deployment  and cleaning ######
macx{
# POST_TARGETDEPS += channels \
#  		macdeploy
channel.target	= channels
channel.commands	= (test -d $$OUT_PWD/build/HsKiste.app/Contents/Resources/ || mkdir -p $$OUT_PWD/build/HsKiste.app/Contents/Resources/) && \
		${COPY} $$PWD/data/channels ${OBJECTS_DIR}/HsKiste.app/Contents/Resources/channels
lang.target	= lang
lang.commands	= (test -d $$OUT_PWD/build/HsKiste.app/Contents/Resources/lang || mkdir -p $$OUT_PWD/build/HsKiste.app/Contents/Resources/lang) && \
		${COPY} $$PWD/data/lang/* ${OBJECTS_DIR}/HsKiste.app/Contents/Resources/lang/
		
addons.target	= addon
addons.commands	= (test -d $$OUT_PWD/build/HsKiste.app/Contents/addons || mkdir -p $$OUT_PWD/build/HsKiste.app/Contents/addons) && \
		${COPY} -R $$PWD/addons/* ${OBJECTS_DIR}/HsKiste.app/Contents/addons
		
nib.target = nib
nib.commands = (test -d $$OUT_PWD/build/HsKiste.app/Contents/Resources || mkdir -p $$OUT_PWD/build/HsKiste.app/Contents/Resources) && \
		${COPY} -R /opt/local/share/qt4/qt_menu.nib ${OBJECTS_DIR}/HsKiste.app/Contents/Resources/qt_menu.nib
# mdepl.target	= ${OBJECTS_DIR}/HsKiste.app/Contents/Resources/qt.conf
mdepl.target	= macdeploy
mdepl.commands	= cd ${OBJECTS_DIR} && macdeployqt HsKiste.app -dmg -verbose=2  && cd ..
mdepl.depends	= all channels lang addons nib
macclean.commands	= rm -rf $$OUT_PWD/build/HsKiste.app $$OUT_PWD/build/HsKiste.dmg
macclean.target	= macclean
compiler_clean.depends	+= macclean
QMAKE_EXTRA_TARGETS += channel mdepl macclean lang addons nib
}
### end mac-deployment and cleaning ######

### begin win32-deployment  and cleaning ######
# moved to main/main.pro
### end win32-deployment and cleaning ######



### begin installation (ubuntu,debian and maemo) ######
!maemo5 {
INSTALLS    += hskiste
hskiste.files  = $$OUT_PWD/build/hskiste
hskiste.path  = /usr/bin/

INSTALLS	+= hstrt
hstrt.path  = /usr/bin/
hstrt.files  = $$OUT_PWD/build/hstarter

INSTALLS    += menudeb
menudeb.path  = /usr/share/menu/
menudeb.files  = $$PWD/data/hskiste

INSTALLS    += menu
menu.path  = /usr/share/applications/
menu.files  = $$PWD/data/hskiste.desktop
}


maemo5 {
INSTALLS    += hskiste
hskiste.files  = $$OUT_PWD/build/hskiste
hskiste.path  = /opt/maemo/usr/bin/

INSTALLS	+= hstrt
hstrt.path  = /opt/maemo/usr/bin/
hstrt.files  = $$OUT_PWD/build/hstarter

}




maemo5 {
INSTALLS    += menu
menu.path  = /usr/share/applications/hildon
menu.files  = $$PWD/data/hskiste.desktop


# INSTALLS    += service
# service.path  = /usr/share/dbus-1/services
# service.files  = $$PWD/data/hskiste.service

INSTALLS    += icon64
icon64.path  = /opt/maemo/usr/share/icons/hicolor/64x64/apps
icon64.files  = $$PWD/data/64x64/hskiste.png

INSTALLS    += appshare
appshare.path  = /opt/maemo/usr/share/hskiste
appshare.files  = $$PWD/data/LICENSE \
		$$PWD/data/channels \
		$$PWD/data/channels.qml
		
INSTALLS    += lang
lang.path  = /opt/maemo/usr/share/hskiste/lang
lang.files  = $$PWD/data/lang/hslang_en.qm
		
INSTALLS    += addon
addon.path  = /opt/maemo/usr/share/hskiste/addons
addon.files  = $$PWD/addons/hskriminet.qml
}


!maemo5 {
INSTALLS    += icon64
icon64.path  = /usr/share/icons/hicolor/64x64/apps
icon64.files  = $$PWD/data/64x64/hskiste.png
	
	
INSTALLS    += appicons
appicons.path  = /usr/share/hskiste/icons
appicons.files  = $$PWD/data/hskiste.png \
		$$PWD/data/done.png \
		$$PWD/data/now.png \
		$$PWD/data/todo.png \
		$$PWD/data/notsaved.png

INSTALLS    += appshare
appshare.path  = /usr/share/hskiste
appshare.files  = $$PWD/data/LICENSE \
		$$PWD/data/channels \
		$$PWD/data/channels.qml
		
INSTALLS    += lang
lang.path  = /usr/share/hskiste/lang
lang.files  = $$PWD/data/lang/hslang_en.qm

INSTALLS    += addon
addon.path  = /usr/share/hskiste/addons
addon.files  = $$PWD/addons/hskriminet.qml
}
		

### end installation (ubuntu,debian and mamemo) ######


### begin targets for debian source and binary package creation #######
debian-src.commands = dpkg-buildpackage -S -r -us -uc -d
debian-bin.commands = dpkg-buildpackage -b -r -uc -d
debian-all.depends = debian-src debian-bin
QMAKE_EXTRA_TARGETS += debian-all debian-src debian-bin compiler_clean
### end targets for debian source and binary package creation ########

#
# Clean all but Makefile
#
compiler_clean.commands = -$(DEL_FILE) $(TARGET)


