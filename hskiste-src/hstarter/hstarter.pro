

TEMPLATE	= app
TARGET  = hstarter



# All generated files into one directory
DESTDIR	= $$OUT_PWD/../build
OBJECTS_DIR = $$OUT_PWD/build
MOC_DIR	= $$OUT_PWD/build
UI_DIR	= $$OUT_PWD/build
VPATH	+= $$PWD/../src $$PWD/../uis
macx:CONFIG -= app_bundle
QT	+= core gui qml network concurrent
maemo5:QT	+=  maemo5


HEADERS	+= common.h
SOURCES	+= starter.cpp \
	common.cpp



DEPENDPATH +=
INCLUDEPATH += $$PWD/../src \
		$$OUT_PWD/build
LIBS	+=
DEFINES	+=



maemo5 {
LIBS	+= -lalarm
}


