﻿import QtQuick 2.3
import hskiste 1.1
import QtQuick.Controls 1.2

/* This is an AddOn to Hörspielkiste (short hskiste), (C) 2010 Frank Fuhlbrück, licensed unter the Terms of GPLv3.
 * It uses the following mechanism:
 * 1. call hsAddOn::getHttpRessource(...) to start fetching a csv file
 * 2. react on signal hsAddOn::httpReplyChanged: parse the reply and create tasks ...
 * 3. ... using hsAddOn::addTask() and append the last added task to a ListModel
 * 4. let the user modify the boolean record property of some Tasks (s. common.h and common.cpp)
 * 5. and finally call hsAddOn::sendTasks() (which adds the selected tasks to hskiste's list) and emit hsAddOn::finished() 
*/


HsAddOn {
	name : "hoerspielkrimi.net"
	id : addOn
    implicitWidth: 800
    implicitHeight: 480
    anchors.fill : parent;
	clip : true;
	onHttpReplyChanged: reactOnReply();
	focus : false;
	

	function reactOnReply(){
		//this does not contain a "full" csv parser (e.g.: escaped quotes are not recognized since hoerspielkrimi.net didn't use them)
		
		var toTasks = httpReply.split("\n");
		var even = false;//to color the lists's lines 
		for(var i=1;i < toTasks.length;i++){
			var line = toTasks[i];
			var fields = new Array();
			var next = new String("");
			while(line.length != 0){
				if(next = line.match(/^"[^"]+"/)){//quoted field (without double qu. s.a.)
					line = line.substring(next[0].length+1);
					next[0] = next[0].substring(1,next[0].length-1);
					fields.push(next[0]);

				} else {//unqoted field or nothing
					next = line.match(/[^,]*,/);
					if(!next || !next[0]){
						break;
					} else {
					line = line.substring(next[0].length);
					next[0] = next[0].substring(0,next[0].length-1);
					fields.push(next[0]);
					}
				}
					
			}
			if(fields.length < 9)
				continue;

			var author = fields[6].match(/Autor\:[^,]*/)[0].substring(6);
			var title = fields[0];
			var chanMnemo = "";
			var start = dateFromString(fields[1] + fields[2],"MM/dd/yyyyhh:mm:ss");
			var stop = dateFromString(fields[3] + fields[4],"MM/dd/yyyyhh:mm:ss");
			
			switch(fields[5]){
				case "BR 2":
					chanMnemo = "BR2";
					break;
				case "DASDING":
					chanMnemo = "DD";
					break;
				case "DLF":
					chanMnemo = "DLF";
					break;
				case "DLR Kultur":
					chanMnemo = "dkultur";
					break;
				case "1Live":
					chanMnemo = "1Live";
					break;
				case "HR 2":
					chanMnemo = "HR2";
					break;
				case "MDR Figaro":
					chanMnemo = "figaro";
					break;
				case "NDR 90,3":
					chanMnemo = "NDR903";
					break;
				case "NDR 1 Niedersachsen":
					chanMnemo = "NDR1NDS";
					break;
				case "NDR Info":
					chanMnemo = "NDRInfo";
					break;
                case "NDR Kultur":
					chanMnemo = "NDRkult";
					break;
				case "RB Nordwestradio":
					chanMnemo = "nwradio";
					break;
				case "RBB radioeins":
					chanMnemo = "radioeins";
					break;
				case "RBB Kulturradio":
					chanMnemo = "RBBKultur";
					break;
				case "SR 1":
					chanMnemo = "SR1";
					break;
				case "SR 2":
					chanMnemo = "SR2";
					break;
				case "SWR 2":
					chanMnemo = "SWR2";
					break;
				case "SWR 4":
					chanMnemo = "SWR4";
					break;
				case "SWR contra":
					chanMnemo = "SWRC"
					break;
				case "WDR 3":
					chanMnemo = "WDR3";
					break;
				case "WDR 5":
					chanMnemo = "WDR5";
					break;
				case "SRF 1":
					chanMnemo = "SRF1";
					break;
				case "SRF 2 Kultur":
					chanMnemo = "SRF2";
					break;
				default:
					chanMnemo = "UNKNOWN:" + fields[5];
				
				/* these channels did not occur: 
				Bremen 4;:;RB4;:;http://sc15.frf.llnw.net:80/stream/gffstream_mp3_w49a;:;0
				DRS 1;:;DRS1;:;rtsp://193.109.53.13:554/encoder/drs1.rm?cloakport=8080,554,7070;:;3
				DRS 2;:;DRS2;:;rtsp://193.109.53.13:554/encoder/drs2.rm?cloakport=8080,554,7070;:;3
				DRS 3;:;DRS3;:;rtsp://193.109.53.13:554/encoder/drs3.rm?cloakport=8080,554,7070;:;3
				DRS Musikwelle;:;DRSMW;:;rtsp://193.109.53.13:554/encoder/mw531.rm?cloakport=8080,554,7070;:;3
				Deutsche Welle;:;DWWorld;:;http://c13010-ls.i.core.cdn.streamfarm.net/dwworldlive-live/13010dwrde64.mp3;:;0
				N-JOY;:;njoy;:;http://ndr.ic.llnwd.net/stream/ndr_n-joy_hi_mp3;:;0
				NDR Kultur;:;NDRkult;:;http://ndr.ic.llnwd.net/stream/ndr_ndrkultur_hi_mp3;:;0
				Ö1;:;Oe1;:;mms://apasf.apa.at/oe1_live_worldwide;:;4
				RBB 88.8;:;88.8;:;mms://stream2.rbb-online.de/wmtencoder/radioberlin-888-live.wma;:;4
				You FM;:;UFM;:;http://gffstream.ic.llnwd.net/stream/gffstream_mp3_w72b;:;0
				BBC Radio 2;:;BBC2;:;rtsp://rmlive.bbc.co.uk/bbc-rbs/rmlive/farm/live24/bbc_ami/radio2/radio2_nb_int_live.ra;:;3
				BBC Radio 3;:;BBC3;:;rtsp://rmlive.bbc.co.uk/bbc-rbs/rmlive/farm/live24/bbc_ami/radio3/radio3_nb_int_live.ra;:;3
				BBC Radio 4;:;BBC4;:;rtsp://rmlive.bbc.co.uk/bbc-rbs/rmlive/farm/live24/bbc_ami/radio4/radio4_nb_int_live.ra;:;3
				*/
			}
			
			addTask(author,title,chanMnemo,start,stop);
			//directly appending the last task would mean appending a copy (s. Qt doc)
			mood.append({"task": tasks[tasks.length-1], "even" : (even = !even)});
		}
	}

	Rectangle {
		id: mainR
		anchors.fill : parent;
		color : "orange"
		clip : true;
			
		Row{	
			id : bline;
			spacing : 12;
			anchors.left: parent.left;
			anchors.leftMargin: 30;
			anchors.top: parent.top;
			anchors.topMargin: 5;
		
		Label {
			text: "Plan für "; 
			anchors.verticalCenter : parent.verticalCenter;
		}
		
		Row{ anchors.verticalCenter : parent.verticalCenter
		ComboBox {
			id: monthBox
			model: ["Januar","Februar","März","April","Mai","Juni",
				"Juli","August","September","Oktober","November","Dezember"]
			currentIndex: new Date().getMonth()
			anchors.verticalCenter : parent.verticalCenter
		}
		SpinBox {
			id: yearBox
			minimumValue: 2000
			maximumValue: 2100
			value: new Date().getFullYear()
			anchors.verticalCenter : parent.verticalCenter
		}
		}
		
		Button { 
			text : "Höspielliste laden" ;
			onClicked: { 
//				var year = Number(yearEd.text) - 2000;
//				year = year >= 10 ? year : "0" + year;
				var month = monthBox.currentIndex + 1;
				month = month >= 10 ? month : "0" + month;
				addOn.getHttpRessource("http://www.hoerspielkrimi.net/archiv/procsv/programm"+ yearBox.value + month + ".csv","ISO 8859-15");
			}
		}
		
		Button { 
			text : "ausgewählte übernehmen" ;
			onClicked: { 
				sendTasks();finished();
			}
		}
		
		Button { 
			text : "Info" ;
			onClicked: { 
				info.visible = !info.visible;
			}
		}
		}
		
		
		Rectangle { id: info; color : "white"; x:100;y:100;z:1; width: 600; height: 300;border.width: 1; visible: false;
			Text { text : "Dieses AddOn zu Hörspielkiste (Lizenz: GPL, (C) 2010) stammt von Frank Fuhlbrück und nutzt die csv-Kalenderdateien von hörspielkrimi.net. Seitens des Autors besteht keine Verbindung zu hörspielkrimi.net (außer Dank für deren Mühe).<br>Sie laden mit diesem AddOn jeweils die Datei http://www.hoerspielkrimi.net/archiv/procsv/<Jahr><Monat>.csv herunter und übernehmen die ausgewählten Termine in die Hörspielkiste.<br>Die Senderkürzel werden entsprechend der \"channels\"-Datei konvertiert (Außnahme: Fernsehsender)." ; 
				width: 580;
				wrapMode: Text.Wrap;
				textFormat : Text.StyledText;
			}
			MouseArea {
				anchors.fill: parent
				onClicked: { info.visible = false;}
			}
		}
		
		Rectangle{
			id : line1
			height: 20;
			anchors.top: bline.bottom;
			anchors.topMargin : 8;
			anchors.left : parent.left;
			anchors.leftMargin : 14;
			anchors.right : parent.right;
			anchors.rightMargin : 8;
			color: "orange";
			Row {
				anchors.right : parent.right;
				anchors.left : parent.left;
				spacing: 10
				Rectangle {color : "gray"; width : 10; height: 10; radius: 5;
				anchors.verticalCenter : parent.verticalCenter;
				
				}
				Label { id: autLab; text: "Autor" ; width: 8 * autLab.height; clip:true ;textFormat : Text.PlainText;}
				Label { id: titLab; text: "Titel" ; clip:true ;textFormat : Text.PlainText; width:0.8*(parent.width - autLab.width - stLab.width - chanLab.width - 5*parent.spacing);}
				Label { id: stLab; text: "Beginn"; width: 4 * autLab.height; clip:true ; textFormat : Text.PlainText;}
				Label { id: chanLab; text: "Sender" ; width: 60; clip:true ;textFormat : Text.PlainText;}
				
				
			}
			
		}
		
		Rectangle{ 
			color : "white";
			anchors.bottom : parent.bottom;
			anchors.bottomMargin : 8;
			anchors.top: line1.bottom;
			anchors.topMargin : 8;
			anchors.left : parent.left;
			anchors.leftMargin : 8;
			anchors.right : parent.right;
			anchors.rightMargin : 8;
			border.width: 1
			radius : 1
			clip: true;

		ListView {
			id : lv
			anchors.fill : parent;
			anchors.margins : 3;
			clip: true;
			boundsBehavior: Flickable.StopAtBounds;
			

			model: ListModel{
				id : mood
			}
			
			
// 			highlight: Rectangle { color: "orange"; radius: 2 }
			
			delegate: Component {
				Rectangle{
					height: 1.2 * autLab.height;
					anchors.right : parent.right;
					anchors.left : parent.left;
					color: even ? "lightblue" : "white";
					
					Row {
						spacing: 10
						Rectangle {
							color : "gray"; 
							width : 10; height: 10; radius: 5; 
							anchors.verticalCenter : parent.verticalCenter;
							
							opacity: task.record ? 1 : 0.1;
						}
						Label { text: task.author ; width: autLab.width; clip:true ;textFormat : Text.PlainText;}
						Label { text: task.title ; width: titLab.width; clip:true ;textFormat : Text.PlainText;}
						Label { text: Qt.formatDateTime(task.start,"dd.MM. hh:mm"); width: 4 * autLab.height; clip:true ; textFormat : Text.PlainText;}
						Label { text: task.chanMnemo ; textFormat : Text.PlainText;}
					}
					MouseArea {
						anchors.fill: parent
						onClicked: {task.record = !task.record;}
					}
				}
				
			}
			
// //			This uses a global MouseArea for the whole listview, but indexAt is buggy a.t.m. 
// //			and the work around may cause undesired results, if the bug is fixed by Qt
// 			MouseArea {
// 				anchors.fill: parent
// 				acceptedButtons: Qt.LeftButton
// 				onClicked: {
// 					if (mouse.button == Qt.LeftButton){
// 						
// 						// maybe the a bug in Qt's indexAt (if scrolled)
// 						
// 						var maxoffs = lv.height / lv.visibleArea.heightRatio;
// 						var offs = maxoffs * lv.visibleArea.yPosition;
// 						console.log(lv.height);
// 						console.log(lv.visibleArea.heightRatio);
// 						console.log(lv.visibleArea.yPosition);
// 						
// 						if(lv.indexAt(mouse.x,offs + mouse.y) != -1  && lv.visibleArea.heightRatio != 0){
// 							mood.get (lv.indexAt(mouse.x,offs + mouse.y)).task.record = 
// 								mood.get (lv.indexAt(mouse.x,offs +mouse.y)).task.record ? false : true;
// // 								lv.currentIndex = lv.indexAt(mouse.x,offs+mouse.y);
// 						} else if(lv.indexAt(mouse.x, mouse.y) != -1){
// 							mood.get (lv.indexAt(mouse.x,mouse.y)).task.record = 
// 								mood.get (lv.indexAt(mouse.x, mouse.y)).task.record ? false : true;
// // 							lv.currentIndex = lv.indexAt(mouse.x, mouse.y);
// 						}
// 					}
// 				}
// 			}
		}
		
		Rectangle {
			id: sb
			color: "gray"
			anchors.right: lv.right
			y: lv.visibleArea.yPosition * lv.height
			width: 10
			height: lv.visibleArea.heightRatio * lv.height
		}

		
		}


	}

}

