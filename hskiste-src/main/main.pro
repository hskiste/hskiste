

TEMPLATE = app
TARGET = hskiste
macx:TARGET= HsKiste


# All generated files into one directory
DESTDIR	= $$OUT_PWD/../build
OBJECTS_DIR = $$OUT_PWD/build
MOC_DIR	= $$OUT_PWD/build
UI_DIR	= $$OUT_PWD/build
VPATH	+= $$PWD/../src $$PWD/../uis
QT	+= core gui qml quick widgets network multimedia webkit concurrent

maemo5:QT	+=  maemo5


HEADERS	+= common.h \
		Listener.h \
		hsAddOn.h \
		MplayerSlave.h
SOURCES	+= main.cpp \
	common.cpp \
	MplayerSlave.cpp
FORMS	+= main.ui \
		about.ui \
		config.ui

win32 {
FORMS	-= config.ui
FORMS	+= w_config.ui
}

maemo5 {
FORMS = 	m_main.ui \
		m_about.ui \
		m_config.ui \
		m_task.ui
}

TRANSLATIONS =	$$PWD/../data/lang/hslang_en.ts
CODECFORTR =	 UTF-8


win32:RC_FILE = $$PWD/../data/icon.rc
macx:ICON=$$PWD/../data/HsKiste.icns
RESOURCES=$$PWD/../hskiste.qrc



#DEPENDPATH +=
INCLUDEPATH += $$PWD/../src \
                $$OUT_PWD/build
#LIBS	+=
#DEFINES	+=

maemo5 {
LIBS	+= -lalarm
}


#### begin win32-deployment  and cleaning ######
win32 {
QMAKE_POST_LINK = windeployqt --qmldir $$PWD/../qml $$shell_path($${DESTDIR}/hskiste.exe)

channel.target	= channels
channel.commands = -$(COPY) $$shell_path($$PWD/../data/channels.qml) $$shell_path($${DESTDIR})

ossl.target	= ossl
ossl.commands = -$(COPY) $$shell_path($$PWD/../*.dll)  $$shell_path($${DESTDIR})

lang.target	= lang
lang.commands	= -$(COPY) $$shell_path($$PWD/../data/lang/*) $$shell_path($${DESTDIR})


addons.target	= addons
addons.commands	= -$(COPY_DIR) $$shell_path($$PWD/../addons) $$shell_path($${DESTDIR}/addons)

QMAKE_EXTRA_TARGETS += channel wdepl lang addons ossl
POST_TARGETDEPS += channels lang addons ossl
}
### end win32-deployment and cleaning ######


#### all other deployment options are in hskiste.pro ######



