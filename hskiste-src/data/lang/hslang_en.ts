<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_GB" sourcelanguage="de_DE">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name></name>
    <message>
        <location filename="../../src/main.cpp" line="474"/>
        <source>am Ende &quot;</source>
        <translation>run &quot;</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="474"/>
        <source>&quot; ausführen</source>
        <translation>&quot; after recording</translation>
    </message>
</context>
<context>
    <name>AbtDlg</name>
    <message>
        <location filename="../../uis/about.ui" line="14"/>
        <location filename="../../uis/m_about.ui" line="14"/>
        <source>Über Hörspielkiste</source>
        <translation>About Hörspielkiste</translation>
    </message>
    <message>
        <location filename="../../uis/about.ui" line="32"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Hörspielkiste&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version 1.0&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Hörspielkiste hilft bleim Planen von Webradioaufnahmen mit cron und mplayer. &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dieses Programm seht unter der GPL v3 (zu finden unter LICENSE).&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;(C) 2009,2010 Frank Fuhlbrück&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Hörspielkiste&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version 1.0&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Hörspielkiste helps you to record your radio plays (and other web radio stuff) using mplayer and cron.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This Application is licensed under GPL v3 (see LICENSE).&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;(C) 2009,2010 Frank Fuhlbrück&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/m_about.ui" line="38"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot; bgcolor=&quot;#000000&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600; color:#ffffff;&quot;&gt;Hörspielkiste&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; color:#ffffff;&quot;&gt;Version 1.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; color:#ffffff;&quot;&gt;Hörspielkiste hilft bleim Planen von Webradioaufnahmen mit cron und mplayer. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; color:#ffffff;&quot;&gt;Dieses Programm seht unter der GPL v3 (zu finden unter LICENSE).&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:16pt; font-weight:600; color:#ffffff;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600; color:#ffffff;&quot;&gt;(C) 2009,2010 Frank Fuhlbrück&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot; bgcolor=&quot;#000000&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600; color:#ffffff;&quot;&gt;Hörspielkiste&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; color:#ffffff;&quot;&gt;Version 1.0&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; color:#ffffff;&quot;&gt;Hörspielkiste helps you to record your radio plays (and other web radio stuff) using mplayer and cron. &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; color:#ffffff;&quot;&gt;This Application is licensed under GPL v3 (see LICENSE).&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:16pt; font-weight:600; color:#ffffff;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:16pt; font-weight:600; color:#ffffff;&quot;&gt;(C) 2009,2010 Frank Fuhlbrück&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>ConfigDlg</name>
    <message>
        <location filename="../../uis/config.ui" line="14"/>
        <location filename="../../uis/m_config.ui" line="14"/>
        <source>Umgebung konfigurieren</source>
        <translation>Configure environment</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="31"/>
        <location filename="../../uis/config.ui" line="89"/>
        <location filename="../../uis/config.ui" line="205"/>
        <location filename="../../uis/config.ui" line="243"/>
        <location filename="../../uis/config.ui" line="281"/>
        <location filename="../../uis/config.ui" line="341"/>
        <location filename="../../uis/config.ui" line="452"/>
        <location filename="../../uis/m_config.ui" line="62"/>
        <location filename="../../uis/m_config.ui" line="109"/>
        <location filename="../../uis/m_config.ui" line="189"/>
        <location filename="../../uis/m_config.ui" line="201"/>
        <location filename="../../uis/m_config.ui" line="248"/>
        <location filename="../../uis/m_config.ui" line="292"/>
        <location filename="../../uis/m_config.ui" line="365"/>
        <source>Datei wählen</source>
        <translation>Choose File</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="44"/>
        <location filename="../../uis/m_config.ui" line="134"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Diese Datei heißt normalerweise &lt;span style=&quot; font-weight:600;&quot;&gt;tasks&lt;/span&gt; und liegt im Order der ausführbaren Dateien von Hörspielkiste.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This file is normally named  &lt;span style=&quot; font-weight:600;&quot;&gt;tasks&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="51"/>
        <location filename="../../uis/m_config.ui" line="141"/>
        <source>Termine</source>
        <translation>tasks</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="64"/>
        <location filename="../../uis/m_config.ui" line="74"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Diese Datei heißt normalerweise &lt;span style=&quot; font-weight:600;&quot;&gt;channels&lt;/span&gt; und liegt im Order der ausführbaren Dateien von Hörspielkiste.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This file is called &lt;span style=&quot; font-weight:600;&quot;&gt;channels&lt;/span&gt; and contains a list of radio channels,their URLs and formats.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="71"/>
        <location filename="../../uis/m_config.ui" line="81"/>
        <source>Senderliste</source>
        <translation>channels</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="218"/>
        <location filename="../../uis/m_config.ui" line="273"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Pfad zur ausführbaren Datei von &lt;span style=&quot; font-weight:600;&quot;&gt;lame&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Path to the &lt;span style=&quot; font-weight:600;&quot;&gt;lame&lt;/span&gt;.executable.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="225"/>
        <location filename="../../uis/m_config.ui" line="280"/>
        <source>lame</source>
        <translation>lame</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="256"/>
        <location filename="../../uis/m_config.ui" line="229"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Pfad zur ausführbaren Datei von &lt;span style=&quot; font-weight:600;&quot;&gt;mplayer&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Path to the &lt;span style=&quot; font-weight:600;&quot;&gt;mplayer&lt;/span&gt; executable.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="263"/>
        <location filename="../../uis/m_config.ui" line="236"/>
        <source>mplayer</source>
        <translation>mplayer</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="316"/>
        <location filename="../../uis/m_config.ui" line="317"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Pfad zur ausführbaren Datei von &lt;span style=&quot; font-weight:600;&quot;&gt;oggenc&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Path to the  &lt;span style=&quot; font-weight:600;&quot;&gt;oggenc&lt;/span&gt;executable.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="323"/>
        <location filename="../../uis/m_config.ui" line="324"/>
        <source>oggenc</source>
        <translation>oggenc</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="354"/>
        <location filename="../../uis/m_config.ui" line="349"/>
        <source>Wo sollen die meisten Hörspiele gespeichert werden?</source>
        <translation type="unfinished">Where should most of the radio plays be saved to?</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="357"/>
        <location filename="../../uis/m_config.ui" line="352"/>
        <source>Standard-
zielverzeichnis</source>
        <translation>target directory</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="371"/>
        <location filename="../../uis/m_config.ui" line="153"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Unter Windows: Die &lt;span style=&quot; font-weight:600;&quot;&gt;crontab.txt&lt;/span&gt; von Pycron.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Unter *nix: Meist liegt diese Datei als &lt;span style=&quot; font-weight:600;&quot;&gt;~/.crontab&lt;/span&gt; im Heimverzeichnis des Nutzers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Windows: Pycron&apos;s &lt;span style=&quot; font-weight:600;&quot;&gt;crontab.txt&lt;/span&gt; .&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;*nix: Usually placed in the uers&apos;s home directory as &lt;span style=&quot; font-weight:600;&quot;&gt;~/.crontab&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="379"/>
        <location filename="../../uis/m_config.ui" line="161"/>
        <source>crontab</source>
        <translation>crontab</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="430"/>
        <location filename="../../uis/m_config.ui" line="390"/>
        <source>Wird nach dem Beeden der Aufzeichnung und evtl. des Codierens ausgeführt. Sinnvoll z.B. um den Rechner herunterzufahren.</source>
        <translation type="unfinished">This command will be executed after recording (and recoding). You could use it e.g. to shut down your computer.</translation>
    </message>
    <message>
        <location filename="../../uis/config.ui" line="433"/>
        <location filename="../../uis/m_config.ui" line="393"/>
        <source>am Ende
ausführen:</source>
        <translation>run after recording:</translation>
    </message>
</context>
<context>
    <name>HKMain</name>
    <message>
        <location filename="../../uis/m_main.ui" line="14"/>
        <location filename="../../uis/main.ui" line="14"/>
        <source>Hörspielkiste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="72"/>
        <location filename="../../uis/main.ui" line="397"/>
        <source>Löschen</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="91"/>
        <location filename="../../uis/main.ui" line="542"/>
        <source>Abbr/Starten</source>
        <translation>Start/Stop</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="110"/>
        <location filename="../../uis/main.ui" line="328"/>
        <source>Neuer Termin</source>
        <translation>New Task</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="128"/>
        <location filename="../../uis/main.ui" line="346"/>
        <source>Sichern</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="140"/>
        <location filename="../../uis/main.ui" line="212"/>
        <source>Titel</source>
        <translation>Title</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="171"/>
        <location filename="../../uis/main.ui" line="199"/>
        <source>Autor</source>
        <translation>Author</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="183"/>
        <location filename="../../uis/main.ui" line="296"/>
        <source>Jobnummer:</source>
        <translation>job number:</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="201"/>
        <location filename="../../uis/main.ui" line="431"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Unter &lt;span style=&quot; font-weight:600;&quot;&gt;Einstellungen &amp;gt; &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;am Ende ausführen &lt;/span&gt;kann dieser Befehl geändert werden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You can modify this command via&lt;span style=&quot; font-weight:600;&quot;&gt;Preferences &amp;gt; run after recording &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="208"/>
        <location filename="../../uis/main.ui" line="438"/>
        <source>Abschlussbefehl ausführen</source>
        <translation>run final command</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="220"/>
        <location filename="../../uis/main.ui" line="67"/>
        <source>Sender</source>
        <translation>Channel</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="241"/>
        <location filename="../../uis/main.ui" line="90"/>
        <source>Startzeitpunkt</source>
        <translation>Start time</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="253"/>
        <location filename="../../uis/main.ui" line="103"/>
        <source>Endzeitpunkt</source>
        <translation>End time</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="317"/>
        <location filename="../../uis/main.ui" line="121"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;nativ&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; - Typ des Streams &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;ohne&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Neucodierung&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;.wav &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;- Sichern als wav/PCM (kein Qualitätsverlust)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;.mp3&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; - (Neu-)Codierung als mp3 (evtl. Qualitätsverlust)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;.ogg &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;- (Neu-)Codierung als ogg/Vorbis (evtl. Qualitätsverlust)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="328"/>
        <location filename="../../uis/main.ui" line="132"/>
        <source>nativ</source>
        <translation>native</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="333"/>
        <location filename="../../uis/main.ui" line="137"/>
        <source>.mp3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="338"/>
        <location filename="../../uis/main.ui" line="142"/>
        <source>.ogg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="343"/>
        <location filename="../../uis/main.ui" line="147"/>
        <source>.wav</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="356"/>
        <location filename="../../uis/main.ui" line="161"/>
        <source>Format/Qualität</source>
        <translation>format/quality</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="368"/>
        <location filename="../../uis/main.ui" line="456"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Da mp3 schlechter codiert als ogg, sollte für mp3 immer ca. eine bis zwei Stufen besser gewählt werden, um dasselbe Resultat zu erreichen.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;mp3&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;: Optimal bis 9 entsprechen den -V Parametern von lame. 10 und mies werden als 9 interpretiert.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;ogg&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;: mit mies als 11 und Optimal als 0  entspricht die Qualitätstufe einem Wert von 10 - x, wenn x der -q Parameter von oggenc ist.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="379"/>
        <location filename="../../uis/main.ui" line="467"/>
        <source>Optimal</source>
        <translation>Optimal</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="384"/>
        <location filename="../../uis/main.ui" line="472"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="389"/>
        <location filename="../../uis/main.ui" line="477"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="394"/>
        <location filename="../../uis/main.ui" line="482"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="399"/>
        <location filename="../../uis/main.ui" line="487"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="404"/>
        <location filename="../../uis/main.ui" line="492"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="409"/>
        <location filename="../../uis/main.ui" line="497"/>
        <source>6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="414"/>
        <location filename="../../uis/main.ui" line="502"/>
        <source>7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="419"/>
        <location filename="../../uis/main.ui" line="507"/>
        <source>8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="424"/>
        <location filename="../../uis/main.ui" line="512"/>
        <source>9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="429"/>
        <location filename="../../uis/main.ui" line="517"/>
        <source>10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="434"/>
        <location filename="../../uis/main.ui" line="522"/>
        <source>mies</source>
        <translation>worst</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="450"/>
        <location filename="../../uis/main.ui" line="283"/>
        <source>Datei</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="468"/>
        <location filename="../../uis/main.ui" line="309"/>
        <source>Num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="486"/>
        <location filename="../../uis/main.ui" line="410"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ist dieses Hörspiel schon aufgezeichnet worden?&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Deaktivieren, &lt;/span&gt;um das Hörspiel (erneut) aufzuzeichnen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="494"/>
        <location filename="../../uis/main.ui" line="418"/>
        <source>bereits aufgenommen</source>
        <translation>already recorded</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="520"/>
        <location filename="../../uis/main.ui" line="247"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In welcher Datei soll das Hörspiel nach dem Aufnehmen und ggf. Codieren gespeichert sein (Zwischenschritte erhalten automatisch passende Dateinamen und werden am Ende gelöscht)?&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Achtung, die Dateiendung wird &lt;span style=&quot; font-weight:600;&quot;&gt;nicht &lt;/span&gt;automatisch &lt;span style=&quot; font-weight:600;&quot;&gt;ergänzt&lt;/span&gt; und &lt;span style=&quot; font-weight:600;&quot;&gt;nicht&lt;/span&gt; &lt;span style=&quot; font-weight:600;&quot;&gt;beachtet &lt;/span&gt;(d.h. ein mp3-codiertes Hörspiel darf z.B. mit .wav enden).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="552"/>
        <location filename="../../uis/main.ui" line="364"/>
        <source>Datei wählen</source>
        <translation>Choose File</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="584"/>
        <location filename="../../uis/main.ui" line="557"/>
        <source>Einstellungen</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="590"/>
        <location filename="../../uis/main.ui" line="563"/>
        <source>Hilfe</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="600"/>
        <location filename="../../uis/main.ui" line="573"/>
        <source>Über Hörspielkiste</source>
        <translation>About Hörspielkiste</translation>
    </message>
    <message>
        <location filename="../../uis/m_main.ui" line="605"/>
        <location filename="../../uis/main.ui" line="578"/>
        <source>Umgebung konfigurieren</source>
        <translation>Configure Environment</translation>
    </message>
</context>
<context>
    <name>Listener</name>
    <message>
        <location filename="../../src/main.cpp" line="73"/>
        <location filename="../../src/main.cpp" line="124"/>
        <location filename="../../src/main.cpp" line="184"/>
        <source>Anhalten</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="73"/>
        <location filename="../../src/main.cpp" line="124"/>
        <location filename="../../src/main.cpp" line="184"/>
        <source>sofort starten</source>
        <translation>Start now</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="385"/>
        <source>am Ende &quot;</source>
        <translation>run &quot;</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="385"/>
        <source>&quot; ausführen</source>
        <translation>&quot; after recording</translation>
    </message>
    <message>
        <source>&quot; ausfÃ¼hren</source>
        <translation type="obsolete">&quot; after recording</translation>
    </message>
</context>
</TS>
