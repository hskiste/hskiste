import hskiste 2.0

ChannelList {
	Channel {
		name: "Bayern 2"
		mnemo: "BR2"
		address: "http://streams.br.de/bayern2nord_2.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "Bremen 4"
		mnemo: "RB4"
		address: "http://dl-ondemand.radiobremen.de/bremenvier.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "DASDING"
		mnemo: "DD"
		address: "http://mp3-live.dasding.de/dasding_m.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "Deutschlandfunk"
		mnemo: "DLF"
		address: "http://stream.dradio.de/7/249/142684/v1/gnl.akacast.akamaistream.net/dradio_mp3_dlf_m"
		type: mp3
		playlist: false
	}
	Channel {
		name: "Deutschlandfunk"
		mnemo: "DLF"
		address: "http://dradio-ogg-dlf-l.akacast.akamaistream.net/7/629/135496/v1/gnl.akacast.akamaistream.net/dradio_ogg_dlf_l"
		type: ogg
		playlist: false
	}
	Channel {
		name: "DeutschlandRadio Kultur"
		mnemo: "dkultur"
		address: "http://stream.dradio.de/7/530/142684/v1/gnl.akacast.akamaistream.net/dradio_mp3_dkultur_m"
		type: mp3
		playlist: false
	}
	Channel {
		name: "DeutschlandRadio Kultur"
		mnemo: "dkultur"
		address: "http://dradio-ogg-dkultur-l.akacast.akamaistream.net/7/978/135496/v1/gnl.akacast.akamaistream.net/dradio_ogg_dkultur_l"
		type: ogg
		playlist: false
	}
	Channel {
		name: "DRadio Wissen"
		mnemo: "dkultur"
		address: "http://stream.dradio.de/7/728/142684/v1/gnl.akacast.akamaistream.net/dradio_mp3_dwissen_m"
		type: mp3
		playlist: false
	}
	Channel {
		name: "DRadio Wissen"
		mnemo: "dkultur"
		address: "http://dradio-ogg-dwissen-l.akacast.akamaistream.net/7/192/135496/v1/gnl.akacast.akamaistream.net/dradio_ogg_dwissen_l"
		type: ogg
		playlist: false
	}
	Channel {
		name: "SRF 1"
		mnemo: "SRF1"
		address: "http://stream.srg-ssr.ch/m/drs1/mp3_128"
		type: mp3
		playlist: false
	}
	Channel {
		name: "SRF 2"
		mnemo: "SRF2"
		address: "http://stream.srg-ssr.ch/m/drs2/mp3_128"
		type: mp3
		playlist: false
	}
	Channel {
		name: "SRF 3"
		mnemo: "SRF3"
		address: "http://stream.srg-ssr.ch/m/drs3/mp3_128"
		type: mp3
		playlist: false
	}
	Channel {
		name: "SRF Musikwelle"
		mnemo: "SFRMW"
		address: "http://stream.srg-ssr.ch/m/drsmw/mp3_128"
		type: mp3
		playlist: false
	}
	Channel {
		name: "EinsLive"
		mnemo: "1Live"
		address: "http://www.wdr.de/wdrlive/media/einslive.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "Hessischer Rundfunk 2"
		mnemo: "HR2"
		address: "http://metafiles.gl-systemhaus.de/hr/hr2_2.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "MDR Figaro"
		mnemo: "figaro"
		address: "http://c22033-l.i.core.cdn.streamfarm.net/22007mdrfigaro/live/3087mdr_figaro/live_de_128.mp3"
		type: mp3
		playlist: false
	}
	Channel {
		name: "N-JOY"
		mnemo: "njoy"
		address: "http://www.ndr.de/resources/metadaten/audio/m3u/n-joy.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "NDR 1 Niedersachsen"
		mnemo: "NDR1NDS"
		address: "http://www.ndr.de/resources/metadaten/audio/m3u/ndr1niedersachsen.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "NDR 90,3"
		mnemo: "NDR903"
		address: "http://www.ndr.de/resources/metadaten/audio/m3u/ndr903.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "NDR Info"
		mnemo: "NDRInfo"
		address: "http://www.ndr.de/resources/metadaten/audio/m3u/ndrinfo.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "NDR Kultur"
		mnemo: "NDRkult"
		address: "http://www.ndr.de/resources/metadaten/audio/m3u/ndrkultur.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "Nordwestradio"
		mnemo: "nwradio"
		address: "http://httpmedia.radiobremen.de/nordwestradio.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "Ö1"
		mnemo: "Oe1"
		address: "http://mp3stream3.apasf.apa.at:8000"
		type: mp3
		playlist: false
	}
	Channel {
		name: "Ö1"
		mnemo: "Oe1"
		address: "mms://apasf.apa.at/oe1_live_worldwide"
		type: wma
		playlist: false
	}
	Channel {
		name: "radioeins"
		mnemo: "RBB1"
		address: "http://www.radioeins.de/live.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "RBB Kulturradio"
		mnemo: "RBBKultur"
		address: "http://www.kulturradio.de/live_s.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "RBB 88.8"
		mnemo: "88.8"
		address: "http://www.radioberlin.de/live.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "SR 1 Europawelle"
		mnemo: "SR1"
		address: "http://streaming01.sr-online.de/sr1_2.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "SR 2 KulturRadio"
		mnemo: "SR2"
		address: "http://streaming01.sr-online.de/sr2_2.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "SWR 2"
		mnemo: "SWR2"
		address: "http://mp3-live.swr.de/swr2_m.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "SWR 4 (BaWü)"
		mnemo: "SWR4"
		address: "http://mp3-live.swr.de/swr4bw_m.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "SWR cont.ra"
		mnemo: "SWRC"
		address: "http://mp3-live.swr.de/contra_m.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "You FM"
		mnemo: "UFM"
		address: "http://metafiles.gl-systemhaus.de/hr/youfm_2.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "WDR 3"
		mnemo: "WDR3"
		address: "http://www.wdr.de/wdrlive/media/wdr3.m3u"
		type: mp3
		playlist: true
	}
	Channel {
		name: "WDR 5"
		mnemo: "WDR5"
		address: "http://www.wdr.de/wdrlive/media/wdr5.m3u"
		type: mp3
		playlist: true
	}
}
