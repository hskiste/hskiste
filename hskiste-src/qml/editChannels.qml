import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
// import QtWebEngine 1.0
import QtWebKit 3.0
import hskiste 2.0
import QtMultimedia 5.0


RowLayout{
	property var types : ["mp3","ogg","wav","real","wma"]
// 	property var typeInc : {"mp3":0,"ogg":1,"wav":2,"real":3,"wma":4}
	anchors.fill : parent
	property int refHgt : 1.5 * refTxt.height
	property int refWdt : refTxt.width
	
	Text {
		id: refTxt
		text: "Reference Text"
		visible: false
	}
	
	TableView {
		id: tv
		property var cl : ChannelList {id: cl}
		model: cl.children
// 		anchors.fill : parent
		onCurrentRowChanged: {selection.clear();selection.select(currentRow)}
		currentRow: 0
		Component.onCompleted: selection.select(0);
		focus: true
		Layout.fillWidth: true
		Layout.fillHeight: true
		
		function swapUp(){
			var r=currentRow;
			var ctrR=rowAt(10,height/2);
			cl.swapChannels(r)
			currentRow=r-1;
			positionViewAtRow(ctrR, ListView.Center)
		}
		
		function swapDown(){
			var r=currentRow;
			var ctrR=rowAt(10,height/2);
			cl.swapChannels(r+1)
			currentRow=r+1;
			positionViewAtRow(ctrR, ListView.Center)
		}
		
		function newChannel(){
			var r=currentRow;
			var ctrR=rowAt(10,height/2);
			cl.newChannel(r)
			currentRow=r;
			positionViewAtRow(ctrR, ListView.Center)
		}
		
		function removeChannel(){
			var r=currentRow;
			var ctrR=rowAt(10,height/2);
			cl.removeChannel(r)
			currentRow=r;
			positionViewAtRow(ctrR, ListView.Center)
		}
		
		Timer {
			id: testTmr
			running: false
			repeat: true
			triggeredOnStart: true
			interval: 6000
			property bool firstIt: true;
			onTriggered: {
				if(tv.currentRow != -1 && !firstIt){
					cl.children[tv.currentRow].error = (med.status == 1 || med.status == 8) && cl.children[tv.currentRow].type <= 2;
				} else {
					firstIt = false;
				}
				if(tv.currentRow == tv.rowCount-1){
					testTmr.stop();
					return;
				}
				tv.currentRow++;
				cl.children[tv.currentRow].test();
				med.stop();
				med.source = cl.children[tv.currentRow].address;
				med.playlist = cl.children[tv.currentRow].playlist;
				med.play();
			}
		}
		
		function testChannels(){
			if(testTmr.running){
				testTmr.stop();
				med.stop();
				med.source = "";
			}
			else {
				if(tv.currentRow != -1)
					tv.currentRow--;
				testTmr.firstIt = true
				testTmr.start()
			}
		}
		
		function testChannelsSilent(){
			for(var i=0;i<tv.rowCount;i++)
				cl.children[i].test();
		}
		
		
		TableViewColumn {
			role: "error"
			title: "Name"
			width: 0.1 * refWdt
			delegate: Text {
				color: "red"
				text: styleData.value? "❗!" :""
			}
		}
		TableViewColumn {
			role: "name"
			title: "Name"
			width: 1.2 * refWdt
			delegate: TextField {
				text: styleData.value
				onTextChanged: cl.children[styleData.row].name = text
			}
		}
		TableViewColumn {
			role: "mnemo"
			title: "Mnemo"
			width:  0.8*refWdt
			delegate: TextField {
				text: styleData.value
				onTextChanged: cl.children[styleData.row].mnemo = text
			}
		}
		TableViewColumn {
			role: "type"
			title: "Typ"
			width: 0.75*refWdt
// 			delegate: Text {
// 				anchors.verticalCenter: parent.verticalCenter
// 				text: types[styleData.value]
// 			}
			delegate: ComboBox {
				anchors.verticalCenter: parent.verticalCenter
				model: types
				currentIndex: styleData.value
				onActivated: cl.children[styleData.row].type = index
			}
			
		}
		
		TableViewColumn {
			role: "address"
			title: "URL"
			width: (parent?parent.width:10 * refWdt) - 4.25 * refWdt - 1.5*saveBtn.width
			delegate: TextField {
				text: styleData.value
				onTextChanged: cl.children[styleData.row].address = text
			}
		}
		TableViewColumn {
			role: "playlist"
			title: "Liste"
			width: 0.45 * refWdt
			delegate: CheckBox {
				x: 0.1*refWdt
				checked: styleData.value
				onCheckedChanged: cl.children[styleData.row].playlist = checked
			}
		}
		TableViewColumn {
			role: ""
			title: ""
			width: 1.1 * refWdt
			delegate: Text {
				text: "im Web suchen"
				MouseArea {
					anchors.fill : parent 
					onClicked : { 
						tv.currentRow = styleData.row;
						searchWdw.visible = true
						wv.url = "https://duckduckgo.com/?q="+cl.children[styleData.row].name+ "+" + types[cl.children[styleData.row].type] + "+stream"
					}
				}
			}
		}
		TableViewColumn {
			role: ""
			title: ""
			width: 0.2*refWdt
			delegate: Text {
				text: playing ? "◼" : "▶"
				property bool playing :  (med.source != "") &&(med.source == cl.children[styleData.row].address)
				MouseArea {
					anchors.fill : parent 
					onClicked : {
						if(!playing){
							med.stop();
							//be careful not to play while the address / playlist configuartion is invalid
							med.source = cl.children[styleData.row].address;
							med.playlist = cl.children[styleData.row].playlist;
							med.play();
						} else {
							med.stop();
							med.source = "";
						}
					}
				}
			}
		}
	}
	Column {
		Button {
			id: newBtn
			text: "neu"
			onClicked: tv.newChannel()
			anchors.left : parent.left
			anchors.right : parent.right
		}
		Button {
			id: delBtn
			text: "löschen"
			onClicked: tv.removeChannel()
			anchors.left : parent.left
			anchors.right : parent.right
		}
		Button {
			id: upBtn
			text: "⇧"
			onClicked: tv.swapUp()
			anchors.left : parent.left
			anchors.right : parent.right
		}
		Button {
			id: dwnBtn
			text: "⇩"
			onClicked: tv.swapDown()
			anchors.left : parent.left
			anchors.right : parent.right
		}
		Rectangle {
			height: 4* refHgt
			width: 3
		}
		Button {
			id: testBtn
			height: 1.5 * refHgt
			text: "Kanäle \ntesten"
			onClicked: tv.testChannels()
			anchors.left : parent.left
			anchors.right : parent.right
		}
		Button {
			id: testSilentBtn
			height: 1.5 * refHgt
			text: "Kanäle still \ntesten"
			onClicked: tv.testChannelsSilent()
			anchors.left : parent.left
			anchors.right : parent.right
		}
		Button {
			id: saveBtn
			height: 1.5 * refHgt
			text: "Liste \nspeichern"
			onClicked: cl.writeChannels()
		}
	}
	
// 	property var med : MediaPlayer {
// 		id: med
// 		onStatusChanged: console.log(status)
// 	}
	property var med : MplayerSlave {
		id: med
		property int status: 0
		onStatusChanged: console.log(status)
	}
	
	property var searchWdw : Window {
		id: searchWdw
		width: 1280
		height: 720
		visible: false
		ColumnLayout {
			anchors.fill: parent
			RowLayout {
				Layout.fillWidth: true
				Button {
					id: back
					text: "<"
					enabled: wv.canGoBack
					onClicked: wv.goBack() 
				}
				Button {
					id: fwd
					text: ">"
					enabled: wv.canGoForward
					onClicked: wv.goForward() 
				}
				TextField {
					id: ab
					onEditingFinished: go.clicked()
					Layout.fillWidth: true
				}
				Button {
					id: go
					text: "⏎"
					onClicked: wv.url=ab.text 
				}
				Button {
					id: useURL
					text: "übernehmen"
					onClicked: {
						searchWdw.visible=false;
						cl.children[tv.currentRow].address = ab.text;
						cl.children[tv.currentRow].playlist = /\.m3u/.test(cl.children[tv.currentRow].address);
							
					}
				}
			}
			WebView {
				id: wv
				Layout.fillWidth: true
				Layout.fillHeight: true
				url: ""
				onLinkHovered: hovUrl=hoveredUrl
				property string hovUrl : ""
				onUrlChanged: ab.text=url
				onNavigationRequested: {
					if(request.url)
						request.action = WebView.AcceptRequest;
				}
				
				MouseArea {
					anchors.fill: parent
					acceptedButtons: Qt.RightButton /*| Qt.LeftButton*/
					onClicked: {
						if(mouse.button == Qt.LeftButton)
							wv.url = wv.hovUrl
						else
							pressAndHold(mouse);
					}
					onPressAndHold: {
						mnLab.urlPopupTime = wv.hovUrl;
						mn.popup();
					}
				}
				
				Menu {
					id: mn
					MenuItem {
						id: mnLab
						property string urlPopupTime: ""
						text: "übernehmen: " + urlPopupTime
						onTriggered:  {
							searchWdw.visible=false;
							cl.children[tv.currentRow].address = urlPopupTime;
							cl.children[tv.currentRow].playlist = /\.m3u/.test(cl.children[tv.currentRow].address);
						}
					}
				}
			}
		}
	}
}