#ifndef HSADDON_H
#define HSADDON_H

#include <QtQuick/QQuickItem>
#include <QtQuick/QQuickView>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include "common.h"

class hsAddOn : public QQuickItem {
	Q_OBJECT
	Q_PROPERTY(QString name READ name WRITE setName)
	Q_PROPERTY(QString httpReply READ httpReply NOTIFY httpReplyChanged())
    Q_PROPERTY(QQmlListProperty<Task> tasks READ readTasks)


public:
    hsAddOn(QQuickItem *parent=0);

	QString name() const;
	void setName(const QString &);


	QString httpReply() const;
    QQmlListProperty<Task> readTasks();

	//the addon may request an http ressource using this function
	//we pass the asynchronous answer to the addon using httpReplyChanged (s.b.)
	Q_INVOKABLE void getHttpRessource(const QString &name, const QString &codec);
	
	//used by the addon to finally submit the tasks (s.b.) to the global list:
	Q_INVOKABLE void sendTasks();
	
	//we store a second list of tasks for the addon and this functions lets the addon add items to that list
	Q_INVOKABLE void addTask(QString author, QString title, QString chanMnemo, QDateTime start, QDateTime stop);
	
	//QML offers date printing funtions but we need to parse foreign dates
	Q_INVOKABLE QDateTime dateFromString(QString date, QString format);

    QQuickView* qv;

public slots:
	//to addon
	void gotReply(QNetworkReply*);

signals:
	//from NetworkAccessManager
	void httpReplyChanged();
	
	//the addon shall force us to emit this signal
	//the sorrounding dialog will be closed once this is emitted (s. main.cpp)
	void finished();




private:
	QString prv_name;
	QString prv_httpReply;
	QString replCodec;
	QList<Task*> prv_tasks;
};

//to have the implemantation at hand it's placed here
//but it won't be included unless HSADDON_H_DEF is defined
#ifdef HSADDON_H_DEF
hsAddOn::hsAddOn(QQuickItem *parent) : QQuickItem(parent)  {};
QString hsAddOn::name() const {
	return prv_name;
};

void hsAddOn::setName(const QString & val) {
	prv_name = val;
};

QString hsAddOn::httpReply() const {
	return prv_httpReply;
};

void hsAddOn::getHttpRessource(const QString &url, const QString &codec) {
	QNetworkAccessManager *nwm = new QNetworkAccessManager(this);
	connect(nwm, SIGNAL(finished(QNetworkReply*)),this, SLOT(gotReply(QNetworkReply*)));
	nwm->get(QNetworkRequest(QUrl(url)));
	replCodec = codec;
};

void hsAddOn::gotReply(QNetworkReply* repl) {
	QTextCodec *codec = QTextCodec::codecForName(replCodec.toUtf8());
	prv_httpReply = codec->toUnicode(repl->readAll().data());
	emit httpReplyChanged();
};


void hsAddOn::sendTasks() {
	foreach(Task* tk, prv_tasks) {
		if (!tk->status)
			continue;
		tk->jobNumber = getNextJobNum();
		tasks.append(*tk);
		QString line = *new QString(tk->title)+ " (" + tk->author+ " "
				+ tk->chanMnemo + "       " + tk->start.toString("dd.MM.   hh:mm")  + ")";
		hkMain->taskList->addItem(new QListWidgetItem(notsavedI,line));
	}
}

QQmlListProperty<Task> hsAddOn::readTasks() {
    return QQmlListProperty<Task>(this, prv_tasks);
}

void hsAddOn::addTask(QString author, QString title, QString chanMnemo, QDateTime start, QDateTime stop) {
	errorS << author << "author" << endl;
	Task* tk = new Task();
	tk->start = start;
	tk->stop = stop;
	tk->author = author;
	tk->title = title;
	tk->chanMnemo = chanMnemo;
	tk->status = DONE;
	prv_tasks.append(tk);
}

QDateTime hsAddOn::dateFromString(QString date, QString format) {
	return QDateTime::fromString(date,format);
}

#endif // HSADDON_H_DEF

#endif // HSADDON_H

