#include <QtCore/QtCore>
#define COMMON
#include "common.h"
#ifdef Q_WS_MAEMO_5
#include <alarmd/libalarm.h>
#include <time.h>
#endif

#include <QtQml/QQmlEngine>
#include <QtQml/QQmlComponent>
#include <QtQml/QQmlContext>
#include <QtConcurrent/QtConcurrent>
#include <QNetworkAccessManager>
#include <QNetworkReply>

QList<Channel*> channels;
QList<Task> tasks;


QString crontab=".crontab";
QString customCrontabCmd="";//e.g. some script that uploads the crontab
QString customHstarterCmd="";//different location of hstarter (e.g. server without access to /usr etc.)

#if defined Q_OS_MAC
QString chanFile="../Resources/channels.qml";
#elif defined Q_WS_MAEMO_5
QString chanFile="/opt/maemo/usr/share/hskiste/channels.qml";
#elif defined Q_OS_LINUX
QString chanFile=PATH_PREFIX "/share/hskiste/channels.qml";
#else
QString chanFile="channels.qml";
#endif

#if defined Q_OS_LINUX || defined Q_OS_WIN32
QString taskFile=QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/tasks";
#else
QString taskFile="tasks";
#endif
QString mplayer="mplayer";
QString lame="lame";
QString oggenc="oggenc";
QString stdDir=".";
QString finalCmd="";

#if defined Q_OS_WIN32
	bool useSchtasks = true;
	bool monthBeforeDay = false;
	QString once = "einmal";
#define SCHTASKSFILE "./cur_schtasks"
#endif


QTextStream errorS(stderr,QIODevice::WriteOnly);
QTextStream outS(stdout,QIODevice::WriteOnly);
QTextStream inS(stdin,QIODevice::ReadOnly);
QTextStream logS;


QDateTime Task::pstart(){
	return start;
}
QDateTime Task::pstop(){
	return stop;
}
QString Task::pauthor(){
	return author;
}
QString Task::ptitle(){
	return title;
}
QString Task::pchanMnemo(){
	return chanMnemo;
}
bool Task::precord(){
	return status;
}

void Task::setStart(const QDateTime val){
	start = val;
}
void Task::setStop(const QDateTime val){
	stop = val;
}
void Task::setAuthor(const QString val){
	author = val;
}
void Task::setTitle(const QString val){
	title = val;
}
void Task::setChanMnemo(const QString  val){
	chanMnemo = val;
}
void Task::setRecord(bool  val){
	if(val == ((bool) status))
		return;
	status = val ? 1 : 0;
	emit recordChanged();
}

Task::Task(){
		jobNumber=0;
		file="";
		ft=-1;
		author="";
		title=tr("NEUER TERMIN");
		chanMnemo="";
		chanType=0;
		status=TODO;
		doFinalCmd=true;
		unsaved=true;
		start=QDateTime::currentDateTime();
		stop=QDateTime::currentDateTime();
}

Task::Task(const Task& tk) :  QObject(){
		jobNumber=tk.jobNumber;
		start = tk.start;
		stop = tk.stop;
		file= tk.file;
		ft= tk.ft;
		author = tk.author;
		title = tk.title;
		chanMnemo = tk.chanMnemo;
		chanType = tk.chanType;
		status = tk.status;
		doFinalCmd =  tk.doFinalCmd;
		unsaved = tk.unsaved;
}

Task Task::operator=(const Task& tk){
	jobNumber=tk.jobNumber;
	start = tk.start;
	stop = tk.stop;
	file= tk.file;
	ft= tk.ft;
	author = tk.author;
	title = tk.title;
	chanMnemo = tk.chanMnemo;
	chanType = tk.chanType;
	status = tk.status;
	doFinalCmd =  tk.doFinalCmd;
	unsaved = tk.unsaved;
	return *this;
}

Channel::Channel(){
	playlist = false;
	error = false;
	type = FMT_MP3;
}


void testWorker(Channel *ch){
	if((ch->type == FMT_WMA) || (ch->type == FMT_REAL))
		return;
	QNetworkAccessManager mngr;
	QNetworkReply *rpl = mngr.get(QNetworkRequest(QUrl(ch->address)));
	//theoreticaly mngr.head would be more sensible here, however we get a lot of aborted connections
	//that do not even send the status code if we only use head instead of get
	QEventLoop loop;
	QObject::connect(rpl, SIGNAL(finished()), &loop, SLOT(quit()));
	QTimer::singleShot(2000, &loop, SLOT(quit()));
	loop.exec();
	
	bool nwErr = rpl->error() != QNetworkReply::NoError;
	int sc = rpl->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
	int ec = 0;
// 	if(!nwErr && sc == 200){
// 		ec = QProcess::execute(mplayer,(QStringList() << "-novideo" << "-nosound" << (ch->playlist?"-playlist":"") << ch->address));
// 	}
	ch->error = (ec != 0 || nwErr || (sc != 200));//ec >0 mplayer errors, ec <0 calling failed-> we may handle each case differently later
	emit ch->errorChanged();
	logS << "tested" << ch->address << ": "  << rpl->error() << " " << sc << "  "<< ec << endl;
	logS.flush();
	delete rpl;
}

void Channel::test(){
	QtConcurrent::run(testWorker,this);
}

ChannelList::ChannelList(QObject* parent) : QObject(parent) {}

void ChannelList::removeChannel(int ind){
	channels.removeAt(ind);
	emit childrenChanged();
	return;
}

void ChannelList::newChannel(int ind/*QVariant var*/){
	channels.insert(ind,new Channel());
	emit childrenChanged();
	return;
}

void ChannelList::swapChannels(int ind){
	if(ind < 1 || ind > (channels.count() -1))
		return;
	channels.swap(ind-1,ind);
	emit childrenChanged();
	return;
}

QQmlListProperty<Channel> ChannelList::children() {
	/* The constructor below is not recommend for production use in docs
	   because the memory of channels is not owned by QML.
	   However, this is exactly what we want here since channels is a global variable */
	return QQmlListProperty<Channel>(this, channels);
}

void initLog(){
	#if defined (Q_WS_MAEMO_5) || defined (Q_OS_LINUX) || defined (Q_OS_MAC)
	QFile *log = new QFile("/tmp/hs.log");
	// #elif defined Q_OS_WIN32
	// QFile *log = new QFile(QDir::homePath() + "\\hs.log");
    #elif defined Q_OS_WIN32
    QDir::root().mkpath(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation));
    QFile *log = new QFile(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/hs.log");
    qDebug() << (QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/hs.log");
	#else
    QFile *log = new QFile("hs.log");
	#endif
	log->open(QIODevice::Append|QIODevice::Text);
	logS.setDevice(log);
    logS << "==== " << QDateTime::currentDateTime().toString(Qt::ISODate) << " ===" << endl;
	logS.flush();
}


Task* getTaskByJobNumber(int jobn){
	for(int i=0; i<tasks.count();i++){
		if(tasks[i].jobNumber == jobn)
			return &(tasks[i]);
	}
	return NULL;
}

Channel* getChannelByMnemoType(QString mnemo, int type){
	for(int i=0; i<channels.count();i++){
		if(channels[i]->mnemo == mnemo && channels[i]->type == type)
			return channels[i];
	}
	return NULL;
}

void readChannels(){
	qmlRegisterType<Channel>("hskiste",2,0, "Channel");
	qmlRegisterType<ChannelList>("hskiste",2,0, "ChannelList");
	
	QQmlEngine eng;
	QQmlContext *cont = eng.rootContext();
	cont->setContextProperty("mp3", QVariant(FMT_MP3));
	cont->setContextProperty("ogg", FMT_OGG);
	cont->setContextProperty("wav", FMT_WAV);
	cont->setContextProperty("real", FMT_REAL);
	cont->setContextProperty("wma", FMT_WMA);
	
	QQmlComponent comp(&eng, QUrl::fromLocalFile(chanFile));

    QList<QQmlError> err = comp.errors();
    for(int i = 0;i< err.length();i++){
        QString eStr = err[i].toString();
        errorS << eStr << endl;
        logS << eStr << endl;
		logS.flush();
    }

	//populates the QList channels
	comp.create(cont);
}

void ChannelList::writeChannels(){
	QFile *chFile = new QFile(chanFile);
// 	if(!(new QFileInfo(chFile)->absoluteDir()).exists())
// 		return;
	chFile->open(QIODevice::WriteOnly|QIODevice::Text);
	QTextStream chStr(chFile);
	chStr << "import hskiste 2.0" << endl << endl;
	chStr << "ChannelList {" << endl;
	for(int i=0; i<channels.count();i++){
		Channel* ch = channels[i];
		chStr << "\tChannel {" << endl;
		chStr << "\t\tname: \"" << ch->name << "\"" << endl;
		chStr << "\t\tmnemo: \"" << ch->mnemo << "\"" << endl;
		chStr << "\t\taddress: \"" << ch->address << "\"" << endl;
		chStr << "\t\ttype: " << typeToString(ch->type) << endl;
		chStr << "\t\tplaylist: " << (ch->playlist?"true":"false") << endl;
		chStr << "\t}" << endl;
	}
	chStr << "}" << endl;
	chFile->close();
}

QList<Task> readTasks(QString taskFile){
	QList<Task> tasks;
	QFile *tkFile = new QFile(taskFile);
	tkFile->open(QIODevice::ReadOnly|QIODevice::Text);
	QTextStream *tkStr = new QTextStream(tkFile);
	
	QString ipline("");
	while (!(ipline = tkStr->readLine()).isNull()){
		QStringList sl =  ipline.split(SEP);
		if(sl.count() != 20)
			continue;
		Task *tk = new Task;
		tk->jobNumber = sl[0].toInt();
		tk->start = QDateTime( *new QDate ( sl[1].toInt(), sl[2].toInt(), sl[3].toInt()), 
				 *new QTime(sl[4].toInt(),sl[5].toInt()));
		tk->stop = QDateTime( *new QDate ( sl[6].toInt(), sl[7].toInt(), sl[8].toInt()), 
				 *new QTime(sl[9].toInt(),sl[10].toInt()));
		tk->file = sl[11];
		tk->ft = sl[12].toInt();
		tk->fq = sl[13].toInt();
		tk->author = sl[14];
		tk->title = sl[15];
		tk->chanMnemo = sl[16];
		tk->chanType = sl[17].toInt();
		tk->status = sl[18].toInt();
		tk->doFinalCmd = (bool) sl[19].toInt();
		tk->unsaved=false;
		tasks.append(*tk);
	}
	tkFile->close();
	return tasks;
}
void  readTasks(){
	tasks = readTasks(taskFile);
}

void writeTasks(QString taskFile, QList<Task> tasks){
	QFile *tkFile = new QFile(taskFile);
	tkFile->open(QIODevice::WriteOnly|QIODevice::Text);
	QTextStream *tkStr = new QTextStream(tkFile);
	
	QString opline("");
	for(int i =0;i< tasks.count();i++){
		Task tk = tasks[i];
		opline = QString::number(tk.jobNumber) + SEP;
		opline += tk.start.toString("yyyy") + SEP;
		opline += tk.start.toString("M") + SEP;
		opline += tk.start.toString("d") + SEP;
		opline += tk.start.toString("h") + SEP;
		opline += tk.start.toString("m") + SEP;
		opline += tk.stop.toString("yyyy") + SEP;
		opline += tk.stop.toString("M") + SEP;
		opline += tk.stop.toString("d") + SEP;
		opline += tk.stop.toString("h") + SEP;
		opline += tk.stop.toString("m") + SEP;
		opline += tk.file + SEP;
		opline += QString::number(tk.ft) + SEP;
		opline += QString::number(tk.fq) + SEP;
		opline += tk.author + SEP;
		opline += tk.title + SEP;
		opline += tk.chanMnemo + SEP;
		opline += QString::number(tk.chanType) + SEP;
		opline += QString::number(tk.status) + SEP;
		opline += QString::number((int)tk.doFinalCmd);
		*tkStr << opline << endl;

	}
	
	tkStr->flush();
	tkFile->close();
}
void writeTasks(){
	writeTasks(taskFile, tasks);
}

void tasksToCrontab(){
	QString exec = "hstarter";
	#ifdef Q_OS_WIN32
		bool win7 = QSysInfo::windowsVersion() >= QSysInfo::WV_VISTA;
		exec += ".exe";
	#endif
	#if defined Q_OS_LINUX
		QString command = PATH_PREFIX "/bin/hstarter";
	#elif defined (Q_OS_MAC) 
		QString command = "\""+QDir::current().absolutePath() + "/" + exec + "\"";
	#elif defined (Q_OS_WIN32)
        QString command;
		if(!useSchtasks)//Windows XP with cron or Windows 7
            command = "\""+QDir::current().absolutePath() + "/" + exec + "\"";
		else if(win7)
            command = "'"+QDir::current().absolutePath() + "/" + exec + "'";
	#else
		QString command = "./" + exec;
	#endif
	if(customHstarterCmd != ""){
		command = customHstarterCmd;
	}

	#if defined Q_WS_MAEMO_5 || defined Q_OS_WIN32
		QString newCookies = "";
	#endif
	

	/* The following conditional blocks distinguish between different systems for planned command invocation.
	 * To avoid code doublication we need interwoven blocks of runtime and compile time ifs.
	 * On some plattforms we only support cron, while on others the user can choose.
	 */
	#if defined Q_WS_MAEMO_5 //uses alarmd exclusively (but cron support might follow)
	
		QList<long> oldCookies;
		QFile crtab(crontab/*TODO: rename*/);
		crtab.open(QIODevice::ReadOnly|QIODevice::Text);
		QTextStream str(&crtab);
		QString ipline("");
		while (!(ipline = str.readLine()).isNull()){
			bool ok=true;
			long ck = ipline.toULong(&ok);
			if(ok)
				oldCookies.append(ck);
		}
		crtab.close();
		
		for (int i = 0; i < oldCookies.size(); ++i)
			alarmd_event_del(oldCookies.at(i));
		
	#else //not Maemo
	QString before,hspart,after,ipline;
	QFile crtab;
	QTextStream str;
	
	#if defined Q_OS_WIN32
	if(useSchtasks){
		
		QStringList oldCookies;
		crtab.setFileName(SCHTASKSFILE);
		crtab.open(QIODevice::ReadOnly|QIODevice::Text);
		QTextStream str(&crtab);
		QString ipline("");
		while (!(ipline = str.readLine()).isNull()){
			if(ipline != "")
				oldCookies.append(ipline);
		}
		crtab.close();
		
		for (int i = 0; i < oldCookies.size(); ++i)
			QProcess::execute("schtasks /Delete /F /TN " + oldCookies.at(i));
		
	} else {//Windows and cron
	#endif //cron (if on Windows then cron instead of schtasks.exe)
	
		before = "";
		after = "";
		hspart = HSBEGIN + QString("\n");
		crtab.setFileName(crontab);
		crtab.open(QIODevice::ReadOnly|QIODevice::Text);
		str.setDevice(&crtab);
		ipline ="";
		while (!(ipline = str.readLine()).isNull() && ipline != HSBEGIN)
			before += ipline + "\n";
		while (!(ipline = str.readLine()).isNull() && ipline != HSEND) ;
		while (!(ipline = str.readLine()).isNull())
			after += ipline + "\n";
		crtab.close();
	
	#if defined Q_OS_WIN32
	} //end: Windows and cron
	#endif
	
	#endif //Maemo or not

	
	
	
	
	for(int i =0; i< tasks.count(); i++){
		Task& tk= tasks[i];
		if(!tk.status)
			continue;
		
		#if defined Q_WS_MAEMO_5
			alarm_event_t *start_aev = alarm_event_create();
			alarm_event_t *stop_aev = alarm_event_create();

			
			start_aev->alarm_time = (time_t) tk.start.toTime_t();
			stop_aev->alarm_time = (time_t) tk.stop.toTime_t();
			
			alarm_event_set_alarm_appid(start_aev,tk.title.toUtf8().data());
			alarm_event_set_alarm_appid(stop_aev,tk.title.toUtf8().data());
			
			alarm_action_t* start_aa =  alarm_event_add_actions(start_aev,1);
			alarm_action_t* stop_aa =  alarm_event_add_actions(stop_aev,1);
			
			alarm_action_set_exec_command(start_aa,(command + " -start " + QString::number(tk.jobNumber)).toUtf8().data());
			alarm_action_set_exec_command(stop_aa,(command + " -stop " + QString::number(tk.jobNumber)).toUtf8().data());
			
			start_aa->flags = stop_aa->flags =  ALARM_ACTION_WHEN_TRIGGERED | ALARM_ACTION_TYPE_EXEC;
			
			long start_ck= alarmd_event_add(start_aev);
			long stop_ck= alarmd_event_add(stop_aev);
			
			
			newCookies += QString::number(start_ck) + "\n" + QString::number(stop_ck) + "\n";
		
		#else
		#if defined Q_OS_WIN32
		if(useSchtasks){
			//the creation time of the task will help to identify it
			QDateTime now = QDateTime::currentDateTime();
			
			
			QString startTN = "hskStart_" + now.time().toString("hhmmss") + "_" + QString::number(tk.jobNumber);
			QString stopTN = "hskStop_" + now.time().toString("hhmmss") + "_" + QString::number(tk.jobNumber);
			
			QString startDate = monthBeforeDay ? tk.start.date().toString("MM/dd/yyyy") : tk.start.date().toString("dd/MM/yyyy");
			QString stopDate = monthBeforeDay ? tk.stop.date().toString("MM/dd/yyyy") : tk.stop.date().toString("dd/MM/yyyy");
			
			QString startTime = tk.start.time().toString("hh:mm:ss");
			QString stopTime = tk.stop.time().toString("hh:mm:ss");
			
			/*		version with explicit path to hskiste (did not work)
			 QProcess::execute("C:/windows/system32/schtasks.exe /Create /RU SYSTEM /TN " + startTN +  
			 " /TR \"" + command + " -start " + QString::number(tk.jobNumber) + "\" /SC " + 
			 once + " /SD " + startDate+ " /ST " + startTime);
			 */
			QString rusystem = "";
			if(!win7)
				rusystem = "/RU SYSTEM";
			QProcess::execute("C:/windows/system32/schtasks.exe /Create "+rusystem+" /TN " + startTN +  
							  " /TR \"" + command + " -start " + QString::number(tk.jobNumber) + "\" /SC " + 
							  once + " /SD " + startDate+ " /ST " + startTime);
			
			logS << "run: " << ("C:/windows/system32/schtasks.exe /Create "+rusystem+" /TN " + stopTN +  
								" /TR \"" + command + " -stop " + QString::number(tk.jobNumber) + "\" /SC " + 
								once + " /SD " + stopDate+ " /ST " + stopTime) << endl;
			
			logS << "exit code: " << QProcess::execute("C:/windows/system32/schtasks.exe /Create "+rusystem+" /TN " + stopTN +  
													   " /TR \"" + command + " -stop " + QString::number(tk.jobNumber) + "\" /SC " + 
													   once + " /SD " + stopDate+ " /ST " + stopTime) << endl;;
			logS.flush();
			
			newCookies += startTN + "\n" + stopTN + "\n";
		} else {//Windows and cron
		#endif //cron (if on Windows then cron instead of schtasks.exe)
		
			hspart += tk.start.toString("m h d M ") + 
				QString::number(tk.start.date().dayOfWeek()) + " " +
				command + " -start " + QString::number(tk.jobNumber) + "\n";
			hspart += tk.stop.toString("m h d M ") +
				QString::number(tk.stop.date().dayOfWeek()) + " " +
				command + " -stop " + QString::number(tk.jobNumber) + "\n";
		#endif
				
		#ifdef Q_OS_WIN32
		}//end: Windows and cron
		#endif
	}
	
	
	
	
	#if defined(Q_WS_MAEMO_5)
	crtab.open(QIODevice::WriteOnly|QIODevice::Text);
	str << newCookies;
	crtab.close();
	#else
	#if defined (Q_OS_WIN32)
	if(useSchtasks){//Windows with schtasks
		logS << newCookies << endl;
		logS << crtab.open(QIODevice::WriteOnly|QIODevice::Text);
		logS.flush();
		str.setDevice(&crtab);
		str << newCookies << endl;
		crtab.flush();
		crtab.close();
	} else {
	#endif
	//platform with cron only (compiletime) or Windows with cron (runtime)
	
		hspart += HSEND + QString("\n");
		crtab.open(QIODevice::WriteOnly|QIODevice::Text);
		str.setDevice(&crtab);
		str << before << hspart << after;
		crtab.close();
	
		
		if(customCrontabCmd != ""){//a custom command is executed on any platform where a crontab is created
			QProcess::execute(customCrontabCmd + " " + crontab);
		} else {
			//on Windows I assume pycron (if not schtasks), which has just one file, it automatically checks
			//for another service like icron, a restart may be required
			#ifdef Q_OS_UNIX
				QProcess::execute("crontab " + crontab);
			#endif
		}
	
	#if defined (Q_OS_WIN32)
	}//final brace for runtime else case (i.e. Windows with cron )
	#endif
	

	#endif //Maemo or not
}

void writeConfig(){
	QSettings  stg("hskiste", "hskiste");
	stg.setValue("mplayer", mplayer);
	stg.setValue("crontab", crontab);
	stg.setValue("customCrontabCmd", customCrontabCmd);
	stg.setValue("customHstarterCmd", customHstarterCmd);
	stg.setValue("chanFile", chanFile);
	stg.setValue("taskFile", taskFile);
	stg.setValue("lame", lame);
	stg.setValue("oggenc", oggenc);
	stg.setValue("stdDir", stdDir);
	stg.setValue("finalCmd", finalCmd);
#ifdef Q_OS_WIN32
	stg.setValue("monthBeforeDay",monthBeforeDay);
	stg.setValue("useSchtasks",useSchtasks);
	stg.setValue("once",once);
#endif
}


void readConfig(){
	QSettings  stg("hskiste", "hskiste");
	mplayer = stg.value("mplayer",mplayer).toString();
	crontab = stg.value("crontab",crontab).toString();
	customCrontabCmd = stg.value("customCrontabCmd",customCrontabCmd).toString();
	customHstarterCmd = stg.value("customHstarterCmd",customHstarterCmd).toString();
	chanFile = stg.value("chanFile",chanFile).toString();
	taskFile = stg.value("taskFile",taskFile).toString();
	lame = stg.value("lame",lame).toString();
	oggenc = stg.value("oggenc",oggenc).toString();
	stdDir = stg.value("stdDir",stdDir).toString();
	finalCmd = stg.value("finalCmd",finalCmd).toString();
#ifdef Q_OS_WIN32
	monthBeforeDay = stg.value("monthBeforeDay",monthBeforeDay).toBool();
	useSchtasks = stg.value("useSchtasks",useSchtasks).toBool();
	once = stg.value("once",once).toString();
#endif
}

QString typeToString(int type){
	switch(type){
	case FMT_MP3:
		return QString("mp3");
	case FMT_OGG:
		return QString("ogg");
	case FMT_WAV:
		return QString("wave");
	case FMT_REAL:
		return QString("real");
	case FMT_WMA:
		return QString("wma");
	}
	return "";
}
