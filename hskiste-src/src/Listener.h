#include <QtCore/QObject>
#include <QtWidgets/QAction>

class Listener : public QObject{
	Q_OBJECT
public slots:
     void updateTasks();
     void chFile();
     void chanSel();
     void taskSel(int idx);
     void taskClicked();
     void ftSel(int idx);
     void guiToTasks();
     void saveTasks();
     void delTask();
     void newTask();
     void immAbb();
     void respMenuAction(QAction* qa);
     void saveConfig();
     void tasksFile();
     void channelsFile();
     void mplFile();
     void cronFile();
     void lameFile();
     void oggFile();
     void stdDirF();
     void initFileName();
     void dateChanged();/*
     void removeChannel(int);
     void newChannel(int);
     void swapChannels(int);*/
}; 
