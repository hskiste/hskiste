#include <QtCore/QObject>
#include <QProcess>

class MplayerSlave : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString source MEMBER source NOTIFY sourceChanged)
	Q_PROPERTY(bool playing READ playing NOTIFY playingChanged)
	Q_PROPERTY(bool playlist MEMBER playlist NOTIFY playlistChanged)
	Q_PROPERTY(bool error MEMBER error NOTIFY errorChanged)
	
public slots:
	Q_INVOKABLE void play();
	Q_INVOKABLE void stop();
	Q_INVOKABLE void restart();
	
public:
	MplayerSlave();
	~MplayerSlave();
	
	QString source;
	bool playing();
	bool playlist;
	bool error;//=false ok in C++11;
	
signals:
	void sourceChanged();
	void playingChanged();
	void playlistChanged();
	void errorChanged();
	
private:
	QProcess* mplProcess;
	bool prv_playing;
};