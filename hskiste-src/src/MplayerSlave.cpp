#include "common.h"
#include "MplayerSlave.h"
//#include <QDebug>

MplayerSlave::MplayerSlave(){
	prv_playing = false;
	playlist = false;
	error = false;
	source = "";
	mplProcess = new QProcess();
	mplProcess->setProgram(mplayer);
	mplProcess->setArguments(QStringList() << "-slave" << "-idle");
	mplProcess->start();
	
	connect(this,SIGNAL(playlistChanged()),this,SLOT(stop()));
	connect(this,SIGNAL(sourceChanged()),this,SLOT(stop()));
	
	logS << "create mplayer" << endl;
	logS.flush();
}

MplayerSlave::~MplayerSlave(){
	logS << "kill mplayer" << endl;
	logS.flush();
	mplProcess->terminate();
	mplProcess->kill();
	delete mplProcess;
}

bool MplayerSlave::playing(){
	return  prv_playing;
}

void MplayerSlave::play(){
	if(!prv_playing)
		restart();
}

void MplayerSlave::stop(){
	if(!prv_playing)
		return;
	
	mplProcess->write("stop\n");
	prv_playing = false;
	emit playingChanged();
}

void MplayerSlave::restart(){
	if(source == ""){
		stop();
		return;
	}
	if(playlist){
		mplProcess->write((QString("loadlist ")+source+ " 0\n").toUtf8().data());
	} else {
		mplProcess->write((QString("loadfile ")+source+ " 0\n").toUtf8().data());
	}
	if(!prv_playing){
		prv_playing = true;
		emit playingChanged();
	}
}

