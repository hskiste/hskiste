#include <QtCore/QtCore>
// #include <QtCore/QObject>
// #include <QtCore/QFile>
// #include <QtCore/QCoreApplication>
#include "common.h"

#ifdef Q_OS_WIN32
#include <windows.h>
#define JOBPREFIX "job"
#endif

#ifdef Q_OS_UNIX
#include <sys/types.h>
#include <signal.h>
#define JOBPREFIX "/tmp/job"
#endif



void kill(QString pid){
#ifdef Q_OS_WIN32
	HANDLE hProcess = OpenProcess( PROCESS_TERMINATE, FALSE, pid.toInt() );
	TerminateProcess( hProcess, (DWORD) -1 );
	CloseHandle( hProcess );

#endif
#ifdef Q_OS_UNIX
	kill(pid.toInt(),SIGTERM);
#endif
	return;
}


void kill(int jobnum){
	QFile jobPid((JOBPREFIX + QString::number(jobnum)) + ".pid");
	jobPid.open(QIODevice::ReadOnly|QIODevice::Text);
	QTextStream str(&jobPid);
	QString pid;
	str >> pid;
	kill(pid);
	jobPid.close();
	jobPid.remove();
}

void createPidFile(int jobnum, qint64 pid){
	QFile jobPid((JOBPREFIX + QString::number(jobnum)) + ".pid");
	jobPid.open(QIODevice::WriteOnly|QIODevice::Text);
	QTextStream str(&jobPid);
	str << QString::number(pid);
	jobPid.close();
}

#ifdef Q_OS_WIN32
QString windowsEnsureShortPath(QString filePath){
	QString dirPath = QFileInfo(filePath).path();
	QDir::root().mkpath(dirPath);
	QFile qf(filePath);
	qf.open(QIODevice::WriteOnly);
	qf.close();
	const LPCTSTR filePathCStr = reinterpret_cast<LPCTSTR>(filePath.utf16());
	int shortLength = GetShortPathNameW(filePathCStr, NULL, 0);
	QScopedArrayPointer<TCHAR> shortBuffer(new TCHAR[shortLength]);
	GetShortPathNameW(filePathCStr, shortBuffer.data(), shortLength);
	QString filePathShort = QString::fromUtf16(reinterpret_cast<const ushort *>(shortBuffer.data()), shortLength - 1);
	return(filePathShort);
}
#endif

int main(int argc, char **argv) {
	QCoreApplication app(argc, argv);
    app.setApplicationName("hskiste");
    app.setOrganizationName("hskiste");
	/*the source file shall be coded in UTF-8
	and therefor all C-Strings should be interpretet as UTF-8*/
	//now standard in Qt5
	//QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
	
	
	QDir::setCurrent (QCoreApplication::applicationDirPath());
	initLog();
	logS << "This is hstarter" << endl;
	logS.flush();
	
	readConfig();
	
	readChannels();
	readTasks();
	
	QStringList args = app.arguments();
	if(args.count() >= 3 &&  args[1] == "-start"){
		int jbn = args[2].toInt();
		logS << "(start) job number: " << jbn << endl;
		Task * tk = getTaskByJobNumber(jbn);
		if(!tk){
			outS << "no such job" << endl;
			logS << "no such job found" << endl;
			return 1;
		}
		Channel * ch = getChannelByMnemoType(tk->chanMnemo,tk->chanType);
		if(!ch){
			outS << "no such channel" << endl;
			logS << "no such channel found" << endl;
			return 1;
		}
		logS << "mplayer path: " << mplayer << endl;
		logS.flush();
		//fclose(stdin);
		QString ext="";
		if(tk->ft != FMT_NAT)
			ext = ".tmp";
		qint64 mplpid;
		QStringList argList; 
		argList << (ch->playlist?"-playlist":"") << ch->address << "-dumpstream" << "-dumpfile";
		#ifdef Q_OS_WIN32 //mplayer on Windows has problems handling non-ASCII paths
			argList << QDir::toNativeSeparators(windowsEnsureShortPath(tk->file + ext));
		#else
			argList << QDir::toNativeSeparators(tk->file + ext);
		#endif
		QProcess::startDetached(mplayer,argList,QDir::current().absolutePath(), &mplpid);
		createPidFile(jbn, mplpid);
		logS << "Recording to: " << QDir::toNativeSeparators(tk->file + ext) << endl;
		logS << "PID of mplayer: " << mplpid << endl;
		logS << "Arguments: " << argList.join(" ") << endl;
		logS.flush();
		tk->status = NOW;
		QList<Task> updTk;
		updTk << *tk;
		writeTasks("update",updTk);
		writeTasks();
		return 0;
		
	} else if(args.count() >= 3 &&  args[1] == "-stop"){
		int jbn = args[2].toInt();
		logS << "(stop) job number: " << jbn << endl;
		logS.flush();
		kill(jbn);
		Task * tk = getTaskByJobNumber(jbn);
		if(!tk){
			outS << "no such job" << endl;
			logS << "no such job found" << endl;
			logS.flush();
			return 1;
		}
		tk->status = DONE;
		QList<Task> updTk;
		updTk << *tk;
		writeTasks("update",updTk);
		writeTasks();
		tasksToCrontab();
		if(tk->ft != FMT_NAT){
			logS << "converting to wave/PCM" << endl;
			logS.flush();
			QString ext="";
			if(tk->ft != FMT_WAV)
				ext = ".wav";
			QProcess mplWav;
			mplWav.start(mplayer, (QStringList() << "-ao" << ("pcm:waveheader:fast:file=\"" + 
				QDir::toNativeSeparators(tk->file) + ext + "\"" ) << (tk->file + ".tmp")));
			mplWav.waitForFinished();
			QFile::remove(tk->file + ".tmp");
		}
		if(tk->ft == FMT_OGG){
			QProcess oggP;
			int oqual = 10 - (tk->fq);
			oggP.start(oggenc, (QStringList() << "-q" << QString::number(oqual) << "-t" << tk->title << "-a" << tk->author << "-o" << QDir::toNativeSeparators(tk->file)
				<< QDir::toNativeSeparators(tk->file + ".wav")));
			oggP.waitForFinished();
			QFile::remove(tk->file + ".wav");
		}
		if(tk->ft == FMT_MP3){
			QProcess lameP;
			int mqual = (tk->fq) >= 10 ? 9 : (tk->fq);
			lameP.start(lame, (QStringList() << "-V"+QString::number(mqual) << "--tt" << tk->title << "--ta" << tk->author << QDir::toNativeSeparators(tk->file + ".wav")
				<< QDir::toNativeSeparators(tk->file)));
			lameP.waitForFinished();
			QFile::remove(tk->file + ".wav");
		}
		if(tk->doFinalCmd)
			QProcess::execute(finalCmd);
		return 0;
	} else if(args.count() >= 2 &&  args[1] == "-write"){
		tasksToCrontab();
	} else if(args.count() >= 2 &&  args[1] == "-test"){
		
		qint64 mplpid;
		QProcess::startDetached(mplayer,
			*new QStringList("http://gffstream.ic.llnwd.net/stream/gffstream_w21b"),
			QDir::current().absolutePath(),&mplpid);
		outS << mplpid << endl;
		kill(QString::number(mplpid));
		
		
	} else {
		outS << "syntax: hstarter -start jobnumber|-stop jobnumber|-write" << endl;;
		return 0;
	}
	
 
	return 0;
}
 
