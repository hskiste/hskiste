#include "Listener.h"
#include "MplayerSlave.h"
#include <QtCore/QtCore>
#include <QMetaObject>
#include <QtWidgets/QFileDialog>
#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickView>
#include <QtQuick/QQuickItem>
#include <QtQml/QQmlContext>
#include "common.h"


#ifdef Q_WS_MAEMO_5
#include "ui_m_main.h"
#include "ui_m_config.h"
#include "ui_m_about.h"
#else
#include "ui_main.h"
#include "ui_about.h"
#ifdef Q_OS_WIN32
#include "ui_w_config.h"
#else
#include "ui_config.h"
#endif
#endif

QMainWindow *mainW;
Ui_HKMain *hkMain;
Ui_ConfigDlg *confDlg=NULL;
QQuickView *ecView;
QIcon nowI;
QIcon doneI;
QIcon todoI;
QIcon notsavedI;
QFileSystemWatcher *fsw;

#define HSADDON_H_DEF
#include "hsAddOn.h"

QHash<QString, hsAddOn*> aoHash;


#if defined Q_OS_MAC
QString langDir="../Resources/lang/";
#elif defined Q_WS_MAEMO_5
QString langDir="/opt/maemo/usr/share/hskiste/lang/";
#elif defined Q_OS_LINUX
QString langDir= PATH_PREFIX "/share/hskiste/lang/";
#else
QString langDir="./";
#endif

#if defined Q_OS_MAC
QString addOnPath ="../addons";
#elif defined Q_WS_MAEMO_5
QString addOnPath="/opt/maemo/usr/share/hskiste/addons/";
#elif defined Q_OS_LINUX
QString addOnPath= PATH_PREFIX "/share/hskiste/addons/";
#else
QString addOnPath="./addons";
#endif


bool ignoreChanges = false;

void noUnsavedChanges();
void unsavedChanges();

void fillTasks(QListWidget *taskList);
void Listener::updateTasks(){
	logS << "update changed" << endl;
	logS.flush();
	QList<Task> updTk = readTasks("update");
	for(int i=0; i< updTk.count();i++){
		Task* tk = getTaskByJobNumber(updTk[i].jobNumber);
		*tk = updTk[i];
	}
	int row = hkMain->taskList->currentRow();
	hkMain->taskList->clear();
	fillTasks(hkMain->taskList);
	hkMain->taskList->setCurrentRow(row);
	fsw->addPath("update");
}

void Listener::chFile(){
	hkMain->fileEdit->clear();
	hkMain->fileEdit->insertPlainText(QFileDialog::getSaveFileName(mainW,
		"Zieldatei angeben", "~/", "Audiodateien (*.mp3 *.ogg *.wav)"));
	return;
}

void Listener::immAbb(){
	int row=0;
	if(hkMain->taskList->currentRow() < 0){
		saveTasks();
	} else
		row = hkMain->taskList->currentRow();
	logS << "immAbb" << endl;
	Task *tk = &tasks[row];
	QString exec = "hstarter";
	#ifdef Q_OS_WIN32
		exec += ".exe";
	#endif
	//NOW running -> stop, else start
	QString arg = tk->status == NOW ? " -stop " : " -start ";
	QString command = "\""+QDir::current().absolutePath() + "/" + exec +  + "\"" + arg
		+ QString::number(tk->jobNumber);
	logS << command << endl;
	logS.flush();
	QProcess::startDetached(command);
	tk->status = (tk->status == NOW) ? DONE : NOW;
	
	writeTasks();
	tasksToCrontab();
	hkMain->taskList->currentItem()->setIcon(tk->status == NOW ? nowI : tk->status == DONE ?
		doneI : todoI);
	
	hkMain->immAbb->setText(tk->status == NOW ? tr("Anhalten") : tr("sofort starten"));
	hkMain->immAbb->setIcon(tk->status == NOW ? todoI : nowI);
	hkMain->doneCB->setChecked(!tk->status);
	

}

void Listener::saveTasks(){
	int row = hkMain->taskList->currentRow();
	logS << "saveTask" << endl;
	logS.flush();

	writeTasks();
	tasksToCrontab();
	
	readTasks();
	fillTasks(hkMain->taskList);

	noUnsavedChanges();
	taskSel(row);
	return;
}

void Listener::guiToTasks(){
	if(ignoreChanges)
		return;
	int row=0;
	if(hkMain->taskList->currentRow() < 0){
		newTask();
		row = hkMain->taskList->count()-1;
	} else
		row = hkMain->taskList->currentRow();
	logS << "saveTask" << endl;
	logS.flush();
	Task *tk = &tasks[row];
//	We don't change the job number, 'cause it shouldn't have changed
// 	tk->jobNumber = hkMain->JobNumL->toPlainText().toInt();
	tk->start = hkMain->start->dateTime();
	tk->stop = hkMain->stop->dateTime();
	tk->file = hkMain->fileEdit->toPlainText();
	tk->ft = hkMain->ftBox->currentIndex() - 1;
	tk->fq = hkMain->fqBox->currentIndex();
	tk->author = hkMain->authorEdit->toPlainText();
	tk->title = hkMain->titleEdit->toPlainText();
	tk->status = (int) !hkMain->doneCB->isChecked();
	tk->doFinalCmd = hkMain->finalCmdCB->isChecked();
	tk->unsaved = true;
	if(hkMain->chanBox->currentIndex() >= 0){
		tk->chanMnemo = channels[hkMain->chanBox->currentIndex()]->mnemo;
		tk->chanType = channels[hkMain->chanBox->currentIndex()]->type;
	}
	unsavedChanges();
	hkMain->taskList->currentItem()->setText(tk->title+ " (" + tk->author+ " " 
	+ tk->chanMnemo + "       " + tk->start.toString("dd.MM.   hh:mm") + ")");
	hkMain->taskList->currentItem()->setIcon(notsavedI);
	
	hkMain->immAbb->setText(tk->status == NOW ? tr("Anhalten") : tr("sofort starten"));
}


void Listener::newTask(){
	errorS << "newTask" << endl;
	Task *tk = new Task;
	tk->jobNumber = getNextJobNum();
	hkMain->taskList->addItem(new QListWidgetItem(notsavedI,tk->title));
	tasks.append(*tk);
	taskSel(hkMain->taskList->count()-1);
	unsavedChanges();
	return;
}

void Listener::delTask(){
	if(hkMain->taskList->currentRow() < 0)
		return;
	errorS << "delTask" << endl;
	tasks.removeAt(hkMain->taskList->currentRow());
	hkMain->taskList->clearSelection();
	hkMain->taskSpecificBox->setEnabled(false);
	errorS << hkMain->taskList->currentRow() << endl;
	hkMain->taskList->takeItem(hkMain->taskList->currentRow());	
	writeTasks();
	#ifdef Q_WS_MAEMO_5
	hkMain->page_2->hide();
	#endif
	return;
}

void Listener::taskSel(int idx){
	ignoreChanges = true;
	errorS << "taskSel: " << idx << endl;
	/*it's very important to check this, because takeitem
	causes a rowChanged signal!*/
	if(idx < 0 || idx >= tasks.count())
		return;
	Task tk = tasks[idx];
	hkMain->authorEdit->clear();
	hkMain->authorEdit->insertPlainText(tk.author);
	hkMain->titleEdit->clear();
	hkMain->titleEdit->insertPlainText(tk.title);
	hkMain->fileEdit->clear();
	hkMain->fileEdit->insertPlainText(tk.file);
	hkMain->start->setDateTime(tk.start);
	hkMain->stop->setDateTime(tk.stop);
	int i;
	errorS << tk.chanMnemo<< endl;
	for(i = 0;i<channels.count();i++){
		errorS << channels[i]->mnemo << endl;
		if(channels[i]->mnemo == tk.chanMnemo && channels[i]->type == tk.chanType)
			break;
	}
	if(i < channels.count())
		hkMain->chanBox->setCurrentIndex(i);
	hkMain->ftBox->setCurrentIndex(tk.ft+1);
	hkMain->fqBox->setCurrentIndex(tk.fq);
	#ifndef Q_WS_MAEMO_5
	hkMain->calendar->setSelectedDate(hkMain->start->date());
	#endif
	hkMain->JobNumL->setNum(tk.jobNumber);
	hkMain->doneCB->setChecked(!tk.status);
	hkMain->finalCmdCB->setChecked(tk.doFinalCmd);
	hkMain->immAbb->setText(tk.status == NOW ? tr("Anhalten") : tr("sofort starten"));
	hkMain->immAbb->setIcon(tk.status == NOW ? todoI : nowI);
	
	hkMain->taskSpecificBox->setEnabled(true);
	hkMain->taskList->setCurrentRow(idx);
	ignoreChanges = false;
	return;
}

void Listener::taskClicked(){
	#ifdef Q_WS_MAEMO_5
	hkMain->page_2->setAttribute(Qt::WA_Maemo5StackedWindow);
	hkMain->page_2->setWindowFlags(hkMain->page_2->windowFlags() | Qt::Window);
	hkMain->page_2->show();
	#endif
}

void Listener::chanSel(){
	ftSel(hkMain->ftBox->currentIndex());
	guiToTasks();
	return;
}

void Listener::ftSel(int idx){
	switch(idx-1){
	case FMT_MP3:
		hkMain->fqBox->setCurrentIndex(4);
		hkMain->fqBox->setEnabled(true);
		break;
	case FMT_OGG:
		hkMain->fqBox->setCurrentIndex(5);
		hkMain->fqBox->setEnabled(true);
		break;
	default:
		hkMain->fqBox->setCurrentIndex(0);
		hkMain->fqBox->setEnabled(false);
	}
	errorS << idx << endl;
	
	QString file = hkMain->fileEdit->toPlainText();
	file = file.left(file.lastIndexOf('.'));
	if(file == "")
		return;
	int ft=-1;
	if(hkMain->ftBox->currentIndex() != 0){
		ft = hkMain->ftBox->currentIndex() - 1;	
	}else if(hkMain->chanBox->currentIndex() >= 0){
		ft = channels[hkMain->chanBox->currentIndex()]->type;
	}
	QString suff = (ft != -1) ? suffixes[ft] : "";
	hkMain->fileEdit->clear();
	hkMain->fileEdit->insertPlainText(file+suff);
	guiToTasks();
	return;
}

void Listener::dateChanged(){
	if(hkMain->start->dateTime() > hkMain->stop->dateTime())
		hkMain->stop->setDateTime(hkMain->start->dateTime());
	guiToTasks();
	errorS << "date changed" << endl;
	//unsavedChanges();
	return;
}

void Listener::initFileName(){
	int ft=-1;
	if(hkMain->ftBox->currentIndex() != 0){
		ft = hkMain->ftBox->currentIndex() - 1;	
	}else if(hkMain->chanBox->currentIndex() >= 0){
		ft = channels[hkMain->chanBox->currentIndex()]->type;
	}
	QString suff = (ft != -1) ? suffixes[ft] : "";
	hkMain->fileEdit->clear();
	hkMain->fileEdit->insertPlainText(
		stdDir + "/" + hkMain->titleEdit->toPlainText() + suff);
}

void Listener::respMenuAction(QAction* qa){
	errorS << qa->objectName() << endl;
	if(qa->objectName() == "actionEnvConfig"){
		QWidget* qd = new QWidget(mainW);
		if(!confDlg)
			confDlg = new Ui_ConfigDlg;
		
		confDlg->setupUi(qd);
		
		//Listener l;
		
		QObject::connect(confDlg->ConfigBB, SIGNAL(accepted()),
                      this, SLOT(saveConfig()));
		QObject::connect(confDlg->fChanB, SIGNAL(clicked()),
                      this, SLOT(channelsFile()));
		QObject::connect(confDlg->fMplB, SIGNAL(clicked()),
                      this, SLOT(mplFile()));
		QObject::connect(confDlg->fTaskB, SIGNAL(clicked()),
                      this, SLOT(tasksFile()));
		QObject::connect(confDlg->fCronB, SIGNAL(clicked()),
                      this, SLOT(cronFile()));
		QObject::connect(confDlg->fLameB, SIGNAL(clicked()),
                      this, SLOT(lameFile()));
		QObject::connect(confDlg->fOggB, SIGNAL(clicked()),
                      this, SLOT(oggFile()));
		QObject::connect(confDlg->fStdDirB, SIGNAL(clicked()),
                      this, SLOT(stdDirF()));
		confDlg->fMplEd->insertPlainText(mplayer);
		confDlg->fChanEd->insertPlainText(chanFile);
		confDlg->fTaskEd->insertPlainText(taskFile);
		confDlg->fCronEd->insertPlainText(crontab);
		confDlg->fLameEd->insertPlainText(lame);
		confDlg->fOggEd->insertPlainText(oggenc);
		confDlg->fStdDirEd->insertPlainText(stdDir);
		confDlg->finalEd->insertPlainText(finalCmd);
		#ifdef Q_OS_WIN32
		confDlg->onceEd->insertPlainText(once);
		#endif
		//stacked Window
		#ifdef Q_WS_MAEMO_5
		qd->setAttribute(Qt::WA_Maemo5StackedWindow);
		#endif
		qd->setWindowFlags(qd->windowFlags() | Qt::Window);
		qd->show();
		return;
	}
	if(qa->objectName() == "actionEditChannels"){
		if(!ecView){
			ecView = new QQuickView();
			ecView->setSource(QUrl("qrc:/qml/editChannels.qml"));
			//the following line ensures no mplayer processes after exit
			connect(qApp,SIGNAL(lastWindowClosed()),ecView, SLOT(deleteLater()));
		}

		ecView->show();
		ecView->resize(800,600);
		return;
	}
	if(qa->objectName() == "actionAbout"){
		QWidget *qd = new QWidget(mainW);
		Ui_AbtDlg *ad = new Ui_AbtDlg;
		
		ad->setupUi(qd);
		QString html = ad->textBrowser->toHtml();
		html.replace("HSKISTEVERSION",HSKISTEVERSION);
		html.replace("HSKISTEYEARS",HSKISTEYEARS);
		ad->textBrowser->setHtml(html);
		
		//stacked Window
		#ifdef Q_WS_MAEMO_5
		qd->setAttribute(Qt::WA_Maemo5StackedWindow);
		#endif
		qd->setWindowFlags(qd->windowFlags() | Qt::Window);
		qd->show();
		return;
	}
	if(aoHash.contains(qa->objectName())){
		hsAddOn *aon = aoHash[qa->objectName()];
		QQuickView *qv = aon->qv;

		QObject::connect(aon, SIGNAL(finished()),
			qv, SLOT(close()));
		qv->show();
		return;
	}
}

void Listener::mplFile(){
	QString fn = QFileDialog::getOpenFileName(mainW,
     "Pfad zu mplayer", "/", "alle Dateien (*)");
	if(fn != ""){
		confDlg->fMplEd->clear();
		confDlg->fMplEd->insertPlainText(fn);
	}
	return;
}

void Listener::channelsFile(){
	QString fn = QFileDialog::getOpenFileName(mainW,
     "Senderliste", "/", "alle Dateien (*)");
	if(fn != ""){
		confDlg->fChanEd->clear();
		confDlg->fChanEd->insertPlainText(fn);
	}
	return;
}

void Listener::cronFile(){
	QString fn = QFileDialog::getSaveFileName(mainW,
     "Pfad zu cron(tab)", "/","",NULL,QFileDialog::DontConfirmOverwrite);
	if(fn != ""){
		confDlg->fCronEd->clear();
		confDlg->fCronEd->insertPlainText(fn);
	}
	return;
}

void Listener::lameFile(){
	QString fn = QFileDialog::getOpenFileName(mainW,
     "Pfad zu lame", "/","",NULL,QFileDialog::DontConfirmOverwrite);
	if(fn != ""){
		confDlg->fLameEd->clear();
		confDlg->fLameEd->insertPlainText(fn);
	}
	return;
}

void Listener::oggFile(){
	QString fn = QFileDialog::getOpenFileName(mainW,
     "Pfad zu oggenc", "/","",NULL);
	if(fn != ""){
		confDlg->fOggEd->clear();
		confDlg->fOggEd->insertPlainText(fn);
	}
	return;
}

void Listener::stdDirF(){
	QString fn = QFileDialog::getExistingDirectory(mainW,
     "Standardzielverzeichnis","/");
	if(fn != ""){
		confDlg->fStdDirEd->clear();
		confDlg->fStdDirEd->insertPlainText(fn);
	}
	return;
}


void Listener::tasksFile(){
	QString fn = QFileDialog::getSaveFileName(mainW,
     "Termindatei", "/", "",NULL,QFileDialog::DontConfirmOverwrite);
	if(fn != ""){
		confDlg->fTaskEd->clear();
		confDlg->fTaskEd->insertPlainText(fn);
	}
	return;
}

void Listener::saveConfig(){
	errorS << "saveConfig" << endl;
	mplayer = confDlg->fMplEd->toPlainText();
	chanFile = confDlg->fChanEd->toPlainText();
	crontab = confDlg->fCronEd->toPlainText();
	taskFile = confDlg->fTaskEd->toPlainText();
	lame = confDlg->fLameEd->toPlainText();
	oggenc = confDlg->fOggEd->toPlainText();
	stdDir = confDlg->fStdDirEd->toPlainText();
	finalCmd = confDlg->finalEd->toPlainText();
#ifdef Q_OS_WIN32
	once = confDlg->onceEd->toPlainText();
	monthBeforeDay = confDlg->monthBefDayCB->isChecked();
	useSchtasks = confDlg->useSchtasksCB->isChecked();
#endif
	if(finalCmd != "")
		hkMain->finalCmdCB->setText(tr("am Ende \"")+ finalCmd +tr("\" ausführen"));
	else
		hkMain->finalCmdCB->setText(tr("Abschlussbefehl ausführen"));
	writeConfig();
	return;
}

void unsavedChanges(){
	hkMain->saveTask->setStyleSheet("background-color:rgb(255, 0, 0);\ncolor: white");
}

void noUnsavedChanges(){
	hkMain->saveTask->setStyleSheet("");
}


void fillChannels(QComboBox *chanBox){
	readChannels();
	for(int i=0; i< channels.count();i++)
		chanBox->addItem(*new QString(channels[i]->name + " ("+typeToString(channels[i]->type))+")");
}

void fillTasks(QListWidget *taskList){
	taskList->clear();
	for(int i=0; i< tasks.count();i++){
		QString line = *new QString(tasks[i].title)+ " (" + tasks[i].author+ " " 
			+ tasks[i].chanMnemo + "       " + tasks[i].start.toString("dd.MM.   hh:mm")  + ")";
		/*if(tasks[i].unsaved)
			taskList->addItem(*new QString(QObject::tr("neu")));
		else */if(tasks[i].status == NOW)
			taskList->addItem(new QListWidgetItem(nowI,line));
		else if (tasks[i].status == TODO)
			taskList->addItem(new QListWidgetItem(todoI,line));
		else 
			taskList->addItem(new QListWidgetItem(doneI,line));
	}
}



int getNextJobNum(){
	/* if we have n jobs, at least one of [1,n+1] is free */
	bool *used = new bool[tasks.count()+1];
	for(int j =0;j<= tasks.count();j++)
		used[j] = false;
	/*the following also works if count=0  
	so no special test is needed */
	for(int i=0; i<tasks.count();i++){
		if(tasks[i].jobNumber <= tasks.count() && tasks[i].jobNumber > 0)
			used[tasks[i].jobNumber] = true;
	}
	int j;
	/*the first unused is just fine*/
	for(j = 1;j<= tasks.count();j++)
		if(!used[j])
			break;
	return j;
}

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	app.setApplicationName("hskiste");
	app.setOrganizationName("hskiste");
	/*the source file shall be coded in UTF-8
	and therefor all C-Strings should be interpreted as UTF-8*/
	//now standard in Qt5
	//QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
	//QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
	
	QString lc = QLocale::system().name();
	QTranslator trtlr;
	trtlr.load(QString("hslang_") + lc,langDir);
	app.installTranslator(&trtlr);
	
	/* in an .app we have to deliver a plugin (s. Makefile),
	which will be the only lib loaded on demand */
#ifdef OSX_APP
	QDir dir(QApplication::applicationDirPath());
	dir.cdUp();
	dir.cd("plugins");
	QApplication::setLibraryPaths(QStringList(dir.absolutePath()));
#endif
 	QDir::setCurrent (QApplication::applicationDirPath());
	initLog();
	logS << "This is hskiste" << endl;
	logS.flush();
	app.setWindowIcon(QIcon(":/data/hskiste.png"));
	
	todoI = QIcon(":/data/todo.png");
	doneI = QIcon(":/data/done.png");
	nowI = QIcon(":/data/now.png");
	notsavedI = QIcon(":/data/notsaved.png");
	
	mainW = new QMainWindow;
	hkMain = new Ui_HKMain;
	
	readConfig();
	
	
	hkMain->setupUi(mainW);
	
	if(finalCmd != "")
		hkMain->finalCmdCB->setText(app.translate("","am Ende \"")+ finalCmd +app.translate("","\" ausführen"));
	fillChannels(hkMain->chanBox);
	readTasks();
	fillTasks(hkMain->taskList);
	
	Listener l;
	
	QObject::connect(hkMain->fchButton, SIGNAL(clicked()),
                      &l, SLOT(chFile()));
	QObject::connect(hkMain->titleEdit, SIGNAL(textChanged()),
                      &l, SLOT(initFileName()));
	QObject::connect(hkMain->taskList, SIGNAL(currentRowChanged(int)),
                      &l, SLOT(taskSel(int)));
	QObject::connect(hkMain->taskList, SIGNAL(clicked(QModelIndex)),
			 &l, SLOT(taskClicked()));
	QObject::connect(hkMain->chanBox, SIGNAL(currentIndexChanged(int)),
                      &l, SLOT(chanSel()));
	QObject::connect(hkMain->ftBox, SIGNAL(currentIndexChanged(int)),
                      &l, SLOT(ftSel(int)));
	QObject::connect(hkMain->fqBox, SIGNAL(currentIndexChanged(int)),
			 &l, SLOT(guiToTasks()));
	QObject::connect(hkMain->start, SIGNAL(dateTimeChanged(QDateTime)),
                      &l, SLOT(dateChanged()));
	QObject::connect(hkMain->stop, SIGNAL(dateTimeChanged(QDateTime)),
                      &l, SLOT(dateChanged()));
	QObject::connect(hkMain->immAbb, SIGNAL(clicked()),
                      &l, SLOT(immAbb()));
	QObject::connect(hkMain->delTask, SIGNAL(clicked()),
			 &l, SLOT(delTask()));
	QObject::connect(hkMain->authorEdit, SIGNAL(textChanged()),
			 &l, SLOT(guiToTasks()));
	QObject::connect(hkMain->titleEdit, SIGNAL(textChanged()),
			 &l, SLOT(guiToTasks()));
	QObject::connect(hkMain->fileEdit, SIGNAL(textChanged()),
			 &l, SLOT(guiToTasks()));
	QObject::connect(hkMain->doneCB , SIGNAL(toggled(bool)),
			 &l, SLOT(guiToTasks()));
	QObject::connect(hkMain->finalCmdCB , SIGNAL(toggled(bool)),
			 &l, SLOT(guiToTasks()));
	
	
	QObject::connect(hkMain->saveTask, SIGNAL(clicked()),
                      &l, SLOT(saveTasks()));
	QObject::connect(hkMain->newTask, SIGNAL(clicked()),
                      &l, SLOT(newTask()));
	QObject::connect(hkMain->menubar, SIGNAL(triggered(QAction*)),
                      &l, SLOT(respMenuAction(QAction*)));
	fsw = new QFileSystemWatcher();
	QObject::connect(fsw, SIGNAL(fileChanged(const QString)), &l, SLOT(updateTasks()));
	fsw->addPath("update");
	logS << fsw->files().join("") << endl; 
	logS.flush();
	
	
	//qml type registration for types only used in the main program (i.e. not hstarter)
	qmlRegisterType<hsAddOn>("hskiste", 1,1, "HsAddOn");
	qmlRegisterType<Task>("hskiste", 1,1, "Task");
	qmlRegisterType<MplayerSlave>("hskiste", 2,0, "MplayerSlave");
	QDir addOnDir = QDir(addOnPath);

	
	foreach (QString fileName, addOnDir.entryList(QStringList("*.qml"),QDir::Files)) {
        QQuickView* qv = new QQuickView(QUrl::fromLocalFile(addOnDir.path() +  "/" + fileName));
        QObject *obj = qv->rootObject();

		
		if (obj) {
			hsAddOn *aon = qobject_cast<hsAddOn*>(obj);
			if (aon){
				QAction *act = new QAction(aon->name(), mainW);
				act->setObjectName("action_" + aon->name());
				hkMain->menuAddOns->addAction(act);
                aon->qv = qv;
                qv->resize(aon->implicitWidth(),aon->implicitHeight());
                aoHash.insert("action_" + aon->name(),aon);
			}
        } else {
            delete qv;
        };
	}
	
	//plugin part ends

	#ifdef Q_WS_MAEMO_5
	mainW->setAttribute(Qt::WA_Maemo5StackedWindow);
	#endif
	mainW->show();
	

 
	return app.exec();
	
	//we keep this; just in case ...
	//manually destroy and delete the ecView to make sure to kill mplayer
// 	int rc = app.exec();
// 	if(ecView){
// 		ecView->destroy();
// 		delete ecView;
// 	}
// 	return rc;
}
 
