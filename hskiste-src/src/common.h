#ifndef COMMON_H
#define COMMON_H
#include <cstdio>
#include <QDateTime>
#include <QString>
#include <QTextStream>
#include <QtQml/QQmlListProperty>

#define HSKISTEVERSION "2.0alpha"
#define HSKISTEYEARS "2009,2010,2011,2015"

class Task : public QObject {
#define DONE 0
#define TODO 1
#define NOW 2
	Q_OBJECT
	Q_PROPERTY(QDateTime start READ pstart WRITE setStart NOTIFY startChanged)
	Q_PROPERTY(QDateTime stop READ pstop WRITE setStop NOTIFY stopChanged)
	Q_PROPERTY(QString author READ pauthor WRITE setAuthor NOTIFY authorChanged)
	Q_PROPERTY(QString title READ ptitle WRITE setTitle NOTIFY titleChanged)
	Q_PROPERTY(QString chanMnemo READ pchanMnemo WRITE setChanMnemo  NOTIFY chanMnemoChanged)
	Q_PROPERTY(bool record READ precord WRITE setRecord  NOTIFY recordChanged)

public:
	int jobNumber;
	QDateTime start;
	QDateTime stop;
	QString file;
	int ft;
	int fq;
	QString author;
	QString title;
	QString chanMnemo;
	int chanType;
	int status;
	bool doFinalCmd;
	bool unsaved;

	QDateTime pstart();
	QDateTime pstop();
	QString pauthor();
	QString ptitle();
	QString pchanMnemo();
	bool precord();

	void setStart(const QDateTime);
	void setStop(const QDateTime);
	void setAuthor(const QString);
	void setTitle(const QString);
	void setChanMnemo(const QString);
	void setRecord(bool);


	Task();
	Task(const Task&);
	Task operator=(const Task&);


signals:
	void startChanged();
	void stopChanged();
	void authorChanged();
	void titleChanged();
	void chanMnemoChanged();
	void recordChanged();
};

class Channel : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString name MEMBER name NOTIFY nameChanged)
	Q_PROPERTY(int type MEMBER type NOTIFY typeChanged)
	Q_PROPERTY(QString address MEMBER address NOTIFY addressChanged)
	Q_PROPERTY(QString mnemo MEMBER mnemo NOTIFY mnemoChanged)
	Q_PROPERTY(bool playlist MEMBER playlist NOTIFY playlistChanged)
	Q_PROPERTY(bool error MEMBER error NOTIFY errorChanged)
	
public:
	Channel();
	Channel(QVariantMap map);
	Q_INVOKABLE void test();
	
	QString name;
	int type;
	QString address;
	QString mnemo;
	bool playlist;
	bool error;//=false ok in C++11;
	
signals:
	void nameChanged();
	void typeChanged();
	void addressChanged();
	void mnemoChanged();
	void playlistChanged();
	void errorChanged();
};

class ChannelList : public QObject {
	Q_OBJECT
	Q_PROPERTY(QQmlListProperty<Channel> children READ children NOTIFY childrenChanged)
	Q_CLASSINFO("DefaultProperty", "children")
public:
	ChannelList(QObject *parent = 0);
	QQmlListProperty<Channel> children();
	
	Q_INVOKABLE void removeChannel(int);
	Q_INVOKABLE void newChannel(int);
	Q_INVOKABLE void swapChannels(int);
	Q_INVOKABLE static void writeChannels();
signals:
	void childrenChanged();
};

int getNextJobNum();
void writeTasks();
void writeTasks(QString taskFile, QList<Task> tasks);
QList<Task> readTasks(QString taskFile);
void readTasks();
void readChannels();
void tasksToCrontab();
void interpret_line(QString line);
void readConfig();
void writeConfig();
Task* getTaskByJobNumber(int jobn);
Channel* getChannelByMnemoType(QString mnemo, int type);
QString typeToString(int type);
void initLog();

#define FMT_MP3 0
#define FMT_OGG 1
#define FMT_WAV 2
#define FMT_REAL 3
#define FMT_WMA 4
#define FMT_NAT -1


#define SEP ";:;"
#define DEFSEP "=:="
#define HSBEGIN "##beginhs#"
#define HSEND "##endhs#"

#if defined Q_OS_LINUX
#ifndef PATH_PREFIX
#define PATH_PREFIX "/usr"
#endif
#endif

#ifndef COMMON
extern QTextStream errorS;
extern QTextStream outS;
extern QTextStream inS;
extern QTextStream logS;

extern QString crontab;
extern QString chanFile;
extern QString taskFile;
extern QString mplayer;
extern QString lame;
extern QString oggenc;
extern QString stdDir;
extern QString finalCmd;
#if defined Q_OS_WIN32
extern bool useSchtasks;
extern bool monthBeforeDay;
extern QString once;
#endif
#if defined Q_OS_LINUX
extern QString ppref;
#endif
extern QString suffixes[];
extern QList<Channel*> channels;
extern QList<Task> tasks;
#else
QString suffixes[] = {".mp3",".ogg",".wav",".rm",".wma"};
#endif
#endif //COMMON_H
// kate: indent-mode cstyle; replace-tabs off; tab-width 8; 
